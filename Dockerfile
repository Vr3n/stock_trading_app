# pull the base image.
FROM python:3.8

# Set Environment Variable.
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_16.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt update && apt upgrade -y
RUN apt install nodejs
RUN npm i -g yarn

# set work directory
WORKDIR /code

RUN mkdir -p /deploy/logs
RUN touch /deploy/logs/error.log
RUN touch /deploy/logs/access.log

# Install dependencies
COPY requirements.txt /code/
COPY yarn.lock /code/
COPY package.json /code/
RUN pip install -r requirements.txt
RUN yarn

# Copy project
COPY . /code/