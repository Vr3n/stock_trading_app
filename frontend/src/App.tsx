import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { w3cwebsocket as W3Websocket } from "websocket";
import Navbar from "./Components/Navbar/Navbar";
import HomePage from "./Pages/HomePage";
import ThirdPage from "./Pages/ThirdPage";
import SecondPage from "./Pages/SecondPage";
import { Box, CssBaseline, Paper, Grid } from "@mui/material";
import Watchlist from "./sections/WatchList/Watchlist";
import ProfilePage from "./Pages/ProfilePage";
import PositionsPage from "./Pages/PositionsPage";
import FundsPage from "./Pages/FundsPage";

const App = () => {
  let { path } = useRouteMatch();

  const ws: any = useRef(null);

  const [stockData, setStockData] = useState([]);
  const prevStockData = useRef(stockData);

  // useEffect(() => {
  //   ws.current = new W3Websocket(
  //     "ws://" + window.location.host + "/ws/stocks/"
  //   );

  //   ws.current.onopen = () => console.log("React app Connected!!");

  //   // When messsage comes from web socket it is process and set as newActivityState.
  //   ws.current.onmessage = (message: any) => {
  //     const data = JSON.parse(message.data);
  //     console.log(data);
  //     const activity_json = JSON.parse(data.activity);

  //     console.log("Received data: " + activity_json);

  //     // @ts-ignore
  //     setStockData([activity_json, ...prevStockData.current]);

  //     // @ts-ignore
  //     prevStockData.current = [activity_json, ...prevStockData.current];
  //   };

  //   ws.current.onclose = () => console.log("WebSocket was closed");

  //   return () => {
  //     ws.current.close();
  //   };
  // }, [0]);

  return (
    <>
      <CssBaseline />
      <Navbar url={path} />
      <Box
        sx={{
          margin: "0 auto",
          maxWidth: "95vw",
        }}
      >
        <Paper
          component="main"
          sx={{
            p: (theme) => theme.spacing(3),
            pt: (theme) => theme.spacing(10),
            minHeight: "100vh",
            background: "#8cc64011",
          }}
          elevation={5}
          square
        >
          <Grid container spacing={2}>
            <Grid item xs={12} md={3}>
              <Paper
                component="aside"
                sx={{
                  p: (theme) => theme.spacing(1),
                  minHeight: "90vh",
                }}
                elevation={5}
              >
                <Watchlist />
              </Paper>
            </Grid>
            <Grid item xs={12} md={9}>
              <Switch>
                <Route path={`${path}/funds/`}>
                  <FundsPage />
                </Route>
                <Route path={`${path}/positions/`}>
                  <PositionsPage />
                </Route>
                <Route path={`${path}/profile/`}>
                  <ProfilePage />
                </Route>
                <Route path={`${path}/orders/`}>
                  <SecondPage />
                </Route>
                <Route path={`${path}/`}>
                  <HomePage />
                </Route>
              </Switch>
            </Grid>
          </Grid>
        </Paper>
      </Box>
    </>
  );
};

export default App;
