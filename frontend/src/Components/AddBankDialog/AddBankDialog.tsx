import React, { useEffect } from "react";
import * as Yup from "yup";
import Dialog from "@mui/material/Dialog";
import FormInput from "../FormInput/FormInput";
import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormGroup,
  Stack,
  Typography,
} from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useMutation, useQueryClient } from "react-query";
import { addBankDetail } from "../../api/addBankDetail";
import { updateBankDetail } from "../../api/updateBankDetail";
import { toast } from "react-toastify";

interface AddBankDialogInput {
  bank_name: string;
  account_number: string;
  ifsc_code: string;
}

const schema = Yup.object({
  bank_name: Yup.string().required("Bank name is Required"),
  account_number: Yup.string().required("Account Number is required"),
  ifsc_code: Yup.string().required("IFSC Code is required"),
});

interface AddBankDialogProps {
  open: boolean;
  update?: boolean;
  handleClose: any;
  user_id: any;
  bank_name?: any;
  account_number?: any;
  ifsc_code?: any;
  bank_id?: any;
}

const AddBankDialog: React.FC<AddBankDialogProps> = ({
  open = false,
  handleClose,
  user_id,
  update = false,
  bank_name = "",
  account_number = "",
  ifsc_code = "",
  bank_id = "",
}) => {
  const { control, handleSubmit, setValue } = useForm<AddBankDialogInput>({
    resolver: yupResolver(schema),
    mode: "onChange",
    defaultValues: {
      bank_name: bank_name,
      account_number: account_number,
      ifsc_code: ifsc_code,
    },
  });

  const query = useQueryClient();

  const createBankDetailMutation = useMutation(addBankDetail, {
    onSuccess: () => {
      return query.invalidateQueries("banks");
    },
  });

  const updateBankDetailMutation = useMutation(updateBankDetail, {
    onSuccess: () => {
      return query.invalidateQueries("banks");
    },
  });

  const createMutate = (data: any) =>
    createBankDetailMutation.mutate(
      {
        ...data,
        user: user_id,
      },
      {
        onSuccess: () => {
          toast.success("Bank Details Successfully added!");
          handleClose();
        },
        onError: (err: any) => {
          toast.error(err.message);
        },
      }
    );

  const updateMutate = (data: any) =>
    updateBankDetailMutation.mutate(
      {
        ...data,
        id: bank_id,
        user: user_id,
      },
      {
        onSuccess: () => {
          toast.success("Bank Details Updated Successfully!");
          handleClose();
        },
        onError: (err: any) => {
          toast.error(err.message);
        },
      }
    );

  useEffect(() => {
    setValue("account_number", account_number);
    setValue("bank_name", bank_name);
    setValue("ifsc_code", ifsc_code);
  }, [account_number, bank_name, ifsc_code]);

  const onSubmit = async (data: any) => {
    if (update) {
      updateMutate(data);
    } else {
      createMutate(data);
    }
  };

  return (
    <Dialog
      open={open}
      onClose={(event, reason) => {
        if (reason !== "backdropClick") {
          handleClose();
        }
      }}
      maxWidth="md"
      fullWidth
    >
      <DialogTitle>
        <Typography variant="h6">Add Bank Account</Typography>
      </DialogTitle>
      <DialogContent sx={{ m: 5, p: 2 }}>
        <form>
          <Stack spacing={3}>
            <FormGroup
              sx={{
                mb: (theme) => theme.spacing(1),
                mt: (theme) => theme.spacing(1),
              }}
            >
              <FormInput
                control={control}
                required
                name="bank_name"
                label="Bank Name"
              />
            </FormGroup>
            <FormGroup sx={{ mb: (theme) => theme.spacing(1) }}>
              <FormInput
                control={control}
                required
                name="account_number"
                label="Account Number"
              />
            </FormGroup>
            <FormGroup sx={{ mb: (theme) => theme.spacing(1) }}>
              <FormInput
                control={control}
                required
                name="ifsc_code"
                label="IFSC Code"
              />
            </FormGroup>
          </Stack>
        </form>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handleSubmit(onSubmit)}
          variant="contained"
          color="success"
        >
          {update ? "Update" : "Add"}
        </Button>
        <Button variant="outlined" onClick={handleClose} color="error">
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default AddBankDialog;
