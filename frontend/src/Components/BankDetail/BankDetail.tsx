import React from "react";

import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { useUserBankQuery } from "../../hooks/useUserBankQuery";

const BankDetail = () => {

  const {
    data: userBankData,
    isLoading: isUserBankLoading,
    isError: isUserBankError,
    error: userBankError,
  } = useUserBankQuery((data: any) => data);

  return (
    <Stack spacing={1} divider={<Divider />}>
      <Typography variant="h4" component="h3">
        Bank Detail
      </Typography>
      <Paper
        elevation={2}
        sx={{
          p: 2,
        }}
      >
        <Stack spacing={2}>
          <Stack
            justifyContent="space-between"
            alignItems="flex-start"
            direction="row"
            sx={{ paddingBottom: (theme) => theme.spacing(1) }}
          >
            <Typography variant="body1">Bank Name:</Typography>
            <Typography variant="body1"></Typography>
          </Stack>
          <Stack
            justifyContent="space-between"
            alignItems="flex-start"
            direction="row"
            sx={{ paddingBottom: (theme) => theme.spacing(1) }}
          >
            <Typography variant="body1">Account Number:</Typography>
            <Typography variant="body1"></Typography>
          </Stack>
          <Stack
            justifyContent="space-between"
            alignItems="flex-start"
            direction="row"
            sx={{ paddingBottom: (theme) => theme.spacing(1) }}
          >
            <Typography variant="body1">IFSC CODE: </Typography>
            <Typography variant="body1"></Typography>
          </Stack>
        </Stack>
      </Paper>
    </Stack>
  );
};

export default BankDetail;
