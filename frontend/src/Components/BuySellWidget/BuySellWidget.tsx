import React from "react";
import Stack from "@mui/material/Stack";
import Chip from "@mui/material/Chip";

const BuySellWidget = () => {
  const handleClick = () => {
    console.info("You clicked the Chip.");
  };

  return (
    <Stack direction="row" spacing={1}>
      <Chip
        label="Buy"
        size="small"
        aria-label="Buy Button"
        color="primary"
        onClick={handleClick}
      />
      <Chip
        label="Sell"
        size="small"
        aria-label="Sell Button"
        color="warning"
        onClick={handleClick}
      />
    </Stack>
  );
};

export default BuySellWidget;
