import React from "react";
import ReactApexChart from "react-apexcharts";

type CandleStickChartProps = {
  options: {};
  series: any[];
  height: number;
};

const CandleStickChart: React.FC<CandleStickChartProps> = ({
  options,
  series,
  height,
}) => {
  return (
    <ReactApexChart
      options={options}
      series={series}
      type="candlestick"
      height={height}
    />
  );
};

export default CandleStickChart;

// options={this.state.options}
//           series={this.state.series}
//           type="candlestick"
//           height={350}
