import React from "react";

import InputBase from "@mui/material/InputBase";
import SearchIcon from "@mui/icons-material/Search";
import Box from "@mui/material/Box";

// search: {
//   position: "relative",
//   borderRadius: theme.shape.borderRadius,
//   backgroundColor: fade(theme.palette.common.white, 0.15),
//   "&:hover": {
//     backgroundColor: fade(theme.palette.common.white, 0.25),
//   },
//   marginRight: theme.spacing(2),
//   marginLeft: 0,
//   width: "100%",
//   [theme.breakpoints.up("sm")]: {
//     marginLeft: theme.spacing(3),
//     width: "auto",
//   },
// },
// searchIcon: {
//   width: theme.spacing(7),
//   height: "100%",
//   position: "absolute",
//   pointerEvents: "none",
//   display: "flex",
//   alignItems: "center",
//   justifyContent: "center",
// },
// inputRoot: {
//   color: "inherit",
// },
// inputInput: {
//   padding: theme.spacing(1, 1, 1, 7),
//   transition: theme.transitions.create("width"),
//   width: "100%",
//   [theme.breakpoints.up("md")]: {
//     width: 200,
//   },
// },

type GlobalFilterProps = {
  preGlobalFilteredRows: [];
  globalFilter: string;
  setGlobalFilter: any;
};

const GlobalFilter: React.FC<GlobalFilterProps> = ({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) => {
  const count = preGlobalFilteredRows.length;

  return (
    <Box
      component="div"
      sx={{
        position: "relative",
        borderRadius: (theme) => theme.shape.borderRadius,
        "&:hover": {
          backgroundColor: (theme) => theme.palette.common.grey,
        },
        marginRight: (theme) => theme.spacing(2),
        marginLeft: 0,
        width: "100%",
        sm: {
          width: "auto",
          marginLeft: (theme) => theme.spacing(3),
        },
      }}
    >
      <Box
        component="div"
        sx={{
          width: (theme) => theme.spacing(7),
          height: "100%",
          position: "absolute",
          pointerEvents: "none",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <SearchIcon />
      </Box>
      <InputBase
        value={globalFilter || ""}
        onChange={(e) => {
          setGlobalFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
        }}
        placeholder={`${count} records...`}
        sx={{
          ".MuiInputBase-root": {
            color: "inherit",
          },
          ".MuiInputBase-input": {
            padding: (theme) => theme.spacing(1, 1, 1, 7),
            transition: (theme) => theme.transitions.create("width"),
            width: "100%",
          },
          md: {
            width: 50,
          },
        }}
        inputProps={{ "aria-label": "search" }}
      />
    </Box>
  );
};

export default GlobalFilter;
