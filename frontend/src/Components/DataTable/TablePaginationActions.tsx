import React, { SyntheticEvent } from "react";
import FirstPageIcon from "@mui/icons-material/FirstPage";
import IconButton from "@mui/material/IconButton";
import Box from "@mui/material/Box";
import KeyboardArrowLeft from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRight from "@mui/icons-material/KeyboardArrowRight";
import LastPageIcon from "@mui/icons-material/LastPage";

type TablePaginationActionsProps = {
  count: number;
  onPageChange: any;
  page: number;
  rowsPerPage: number;
};

const TablePaginationActions: React.FC<TablePaginationActionsProps> = ({
  count,
  onPageChange,
  page,
  rowsPerPage,
}) => {
  const handleFirstPageButtonClick = (event: SyntheticEvent) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event: SyntheticEvent) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event: SyntheticEvent) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event: SyntheticEvent) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box
      component="div"
      sx={{
        flexShrink: 0,
      }}
    >
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        <FirstPageIcon />
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="pevious page"
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        <KeyboardArrowRight />
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        <LastPageIcon />
      </IconButton>
    </Box>
  );
};

export default TablePaginationActions;
