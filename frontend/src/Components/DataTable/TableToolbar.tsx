import React from "react";
import OrderDialog from "../OrderDialog/OrderDialog";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Tooltip from "@mui/material/Tooltip";
import DeleteIcon from "@mui/icons-material/Delete";
import GlobalFilter from "./GlobalFilter";
import IconButton from "@mui/material/IconButton";

type TableToolbarProps = {
  numSelected: number;
  deleteHandler?: any;
  setGlobalFilter: any;
  preGlobalFilteredRows: any;
  globalFilter: string;
  dialog?: any;
  title: string;
};

const TableToolbar: React.FC<TableToolbarProps> = ({
  numSelected,
  deleteHandler,
  preGlobalFilteredRows,
  setGlobalFilter,
  globalFilter,
  dialog,
  title,
}) => {
  return (
    <Toolbar
      // className={clsx(classes.root, {
      //   [classes.highlight]: numSelected > 0,
      // })}
      sx={{
        root: {
          paddingLeft: (theme) => theme.spacing(2),
          paddingRight: (theme) => theme.spacing(1),
        },
      }}
    >
      {dialog}
      {numSelected > 0 ? (
        <Typography
          sx={{
            flex: "1 1 100%",
          }}
          color="inherit"
          variant="subtitle1"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{
            flex: "1 1 100%",
          }}
          variant="h6"
          id="tableTitle"
        >
          {title}
        </Typography>
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton aria-label="delete" onClick={deleteHandler}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      ) : (
        <GlobalFilter
          preGlobalFilteredRows={preGlobalFilteredRows}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
        />
      )}
    </Toolbar>
  );
};

export default TableToolbar;
