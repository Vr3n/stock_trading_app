import React from "react";
import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";

type DeleteModalProps = {
  code: string;
  onClose: any;
  open: boolean;
  deleteFunc: any;
};

const DeleteModal: React.FC<DeleteModalProps> = ({
  code,
  open,
  onClose,
  deleteFunc,
}) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">Delete {code}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          Are you sure you want to delete {code} ?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} variant="outlined" color="primary">
          Disagree
        </Button>
        <Button
          variant="contained"
          color="error"
          onClick={() =>
            deleteFunc.mutate(code, {
              onSuccess: () => {
                toast.success(`${code} sucessfully removed from Watchlist`);
                onClose();
              },
              onError: (err: any) => {
                toast.error(err.message);
              },
            })
          }
          autoFocus
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteModal;
