import React from "react";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Dialog from "@mui/material/Dialog";
import Typography from "@mui/material/Typography";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Button from "@mui/material/Button";
import { toast } from "react-toastify";

type DeleteOrderModalProps = {
  orderId: string;
  orderState: "buy" | "sell";
  code: string | null;
  price: string | null;
  stockId?: any;
  stop_loss: any;
  quantity: string | null;
  onClose: any;
  open: boolean;
  deleteFunc: any;
};

type OrderDetailTableProps = {
  orderState: "buy" | "sell";
  code: string | null;
  price: string | null;
  stop_loss: any;
  quantity: string | null;
};

const OrderDetailTable: React.FC<OrderDetailTableProps> = ({
  orderState,
  code,
  price,
  stop_loss,
  quantity,
  margin,
}) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Code</TableCell>
            <TableCell align="right">Price</TableCell>
            <TableCell align="right">Order Action</TableCell>
            <TableCell align="right">Stop Loss</TableCell>
            <TableCell align="right">Quantity</TableCell>
            <TableCell align="right">Margin</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell component="th" scope="row">
              {code}
            </TableCell>
            <TableCell align="right">{price}</TableCell>
            <TableCell align="right">{orderState}</TableCell>
            <TableCell align="right">{stop_loss}</TableCell>
            <TableCell align="right">{quantity}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const DeleteOrderModal: React.FC<DeleteOrderModalProps> = ({
  orderState = "",
  open = false,
  price = 0,
  stockId,
  quantity,
  orderId,
  stop_loss,
  code,
  onClose,
  deleteFunc,
}) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth="lg"
    >
      <DialogTitle id="alert-dialog-title">Delete Order</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-order-details">
          <OrderDetailTable
            orderState={orderState}
            price={price}
            code={code}
            quantity={quantity}
            stop_loss={stop_loss}
          />
        </DialogContentText>
        <DialogContentText id="alert-dialog-description">
          <Typography sx={{ mt: (theme) => theme.spacing(1) }} variant="body1">
            Are you sure you want to delete order ?
          </Typography>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} variant="outlined" color="primary">
          Disagree
        </Button>
        <Button
          variant="contained"
          color="error"
          onClick={() =>
            deleteFunc.mutate(orderId, {
              onSuccess: () => {
                toast.success(`order successfully deleted!`);
                onClose();
              },
              onError: (err: any) => {
                toast.error(err.message);
              },
            })
          }
          autoFocus
        >
          Delete
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default DeleteOrderModal;
