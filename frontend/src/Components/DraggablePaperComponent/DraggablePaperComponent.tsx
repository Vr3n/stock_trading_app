import React from "react";
import Paper, { PaperProps } from "@mui/material/Paper";
import Draggable from "react-draggable";

const DraggablePaperComponent = (props: PaperProps) => {
  return (
    <Draggable
      handle="#draggable-order-dialog-title"
      cancel={'[class*="MuiDialogContent-root"]'}
    >
      <Paper {...props} />
    </Draggable>
  );
};

export default DraggablePaperComponent;
