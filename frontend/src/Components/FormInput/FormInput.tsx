import React from "react";
import { useController } from "react-hook-form";
import TextField from "@mui/material/TextField";

type FormInputProps = {
  label?: string;
  name: string;
  control: any;
  type?: "text" | string;
  disabled?: boolean;
  required?: boolean;
  autoFocus?: boolean;
  defaultValue?: any;
  inputProps?: any;
  fullWidth?: boolean;
};

const FormInput: React.FC<FormInputProps> = ({
  label,
  name,
  control,
  type = "text",
  disabled = false,
  required = false,
  autoFocus = false,
  fullWidth = true,
  defaultValue = undefined,
  inputProps,
}) => {
  const { field, fieldState, formState } = useController({
    control,
    name,
    defaultValue: defaultValue !== undefined ? defaultValue : "",
  });

  return (
    <TextField
      {...field}
      type={type}
      label={label}
      id={name}
      disabled={disabled}
      fullWidth={fullWidth}
      error={fieldState.invalid}
      required={required}
      autoFocus={autoFocus}
      helperText={fieldState.invalid && fieldState.error?.message}
      ref={field.ref}
      inputProps={inputProps}
    />
  );
};

export default FormInput;
