import React from "react";
import { useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import CardHeader from "@mui/material/CardHeader";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import PieChartOutlineOutlinedIcon from "@mui/icons-material/PieChartOutlineOutlined";

const InfoCard = () => {
  const theme = useTheme();

  return (
    <Card>
      <CardHeader
        avatar={<PieChartOutlineOutlinedIcon />}
        title="Equity"
        titleTypographyProps={{
          variant: "h6",
        }}
      />
      <CardContent>
        <Stack
          divider={<Divider orientation="vertical" flexItem />}
          spacing={2}
          direction="row"
        >
          <Box>
            <Typography variant="h3">0.3</Typography>
            <Typography variant="subtitle2" color="text.secondary">
              Margin Available
            </Typography>
          </Box>
          <Box>
            <Typography variant="subtitle2" color="text.secondary">
              Margins Used &nbsp;{" "}
              <span style={{ fontSize: "16px", color: "#000" }}>0</span>
            </Typography>
            <Typography variant="subtitle2" color="text.secondary">
              Opening Balance &nbsp;{" "}
              <span style={{ fontSize: "16px", color: "#000" }}>0.3</span>
            </Typography>
          </Box>
        </Stack>
      </CardContent>
    </Card>
  );
};

export default InfoCard;
