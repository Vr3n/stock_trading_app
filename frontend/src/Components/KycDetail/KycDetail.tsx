import React from "react";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import LinearProgress from "@mui/material/LinearProgress";
import Divider from "@mui/material/Divider";
import { useKycRetriveQuery } from "../../hooks/useKycRetriveQuery";

const KycDetail = () => {
  const {
    data: userKycData,
    isLoading: isUserKycLoading,
    isError: isUserKycError,
    error: kycError,
  } = useKycRetriveQuery((data: any) => data);

  return isUserKycLoading && userKycData === undefined ? (
    <LinearProgress />
  ) : (
    <Paper
      sx={{
        p: 5,
        alignItems: "center",
      }}
    >
      <Stack spacing={2} divider={<Divider />}>
        <Stack
          spacing={1}
          direction="row"
          divider={<Divider orientation="vertical" flexItem />}
        >
          <Typography variant="h5">KYC Details</Typography>
        </Stack>
        <Paper
          sx={{
            p: 5,
            alignItems: "center",
          }}
          elevation={2}
        >
          <Typography gutterBottom variant="h5">
            Document Details
          </Typography>
          <Divider />
          {userKycData[0].is_kyc_verified ? (
            <Stack sx={{ mt: (theme) => theme.spacing(2) }}>
              <Typography variant="body1" gutterBottom>
                Kyc Is verified ✅
              </Typography>
            </Stack>
          ) : (
            <>
              <Stack
                direction="row"
                justifyContent="space-between"
                alignItems="center"
                sx={{ mt: (theme) => theme.spacing(2) }}
              >
                <Typography variant="body1">Kyc Is not verified ❌</Typography>
                <a
                  href={`/trading/kyc/update/${userKycData[0].id}/`}
                  target="_blank"
                  style={{ padding: "5px", backgroundColor: "#3870c9" }}
                >
                  Upload KYC Documents
                </a>
              </Stack>
            </>
          )}
        </Paper>
      </Stack>
    </Paper>
  );
};

export default KycDetail;
