import React from "react";
import Avatar from "@mui/material/Avatar";
import { green } from "@mui/material/colors";

const stringAvatar = (name: string) => {
  return {
    children: `${name.split(" ")[0][0]}${name.split(" ")[1][0]}`,
  };
};

type LetterAvatarProps = {
  name: string;
  size?: string;
};

const LetterAvatar: React.FC<LetterAvatarProps> = ({ name, size = "" }) => {
  let styles: any = {
    backgroundColor: green[500],
  };

  if (size === "small") {
    styles = {
      ...styles,
      width: 30,
      fontSize: 16,
      height: 30,
    };
  }

  return (
    <Avatar alt={`${name} profile pic`} sx={styles} {...stringAvatar(name)} />
  );
};

export default LetterAvatar;
