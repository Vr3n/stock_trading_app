import React from "react";
import Grid from "@mui/material/Grid";
import Divider from "@mui/material/Divider";
import OHLCDetails from "./OHLCDetails";

const MarketDepthWidget = () => {
  return (
    <>
      <Grid
        container
        justifyContent="space-around"
        alignItems="center"
        spacing={1}
      >
        <Grid item xs={6}>
          <table>
            <thead>
              <tr>
                <th align="left" style={{ fontSize: "12px" }}>
                  BID
                </th>
                <th align="center" style={{ fontSize: "12px" }}>
                  ORDERS
                </th>
                <th align="right" style={{ fontSize: "12px" }}>
                  QTY
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "blue", fontSize: "12px" }}>1593.0</td>
                <td align="center" style={{ color: "blue", fontSize: "12px" }}>
                  1
                </td>
                <td style={{ color: "blue", fontSize: "12px" }}>250</td>
              </tr>
            </tbody>
          </table>
        </Grid>
        <Grid item xs={6}>
          <table>
            <thead>
              <tr>
                <th align="left" style={{ fontSize: "12px" }}>
                  OFFER
                </th>
                <th align="center" style={{ fontSize: "12px" }}>
                  ORDERS
                </th>
                <th align="right" style={{ fontSize: "12px" }}>
                  QTY
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
              <tr>
                <td style={{ color: "orange", fontSize: "12px" }}>1593.0</td>
                <td
                  align="center"
                  style={{ color: "orange", fontSize: "12px" }}
                >
                  1
                </td>
                <td style={{ color: "orange", fontSize: "12px" }}>250</td>
              </tr>
            </tbody>
          </table>
        </Grid>
      </Grid>
      <Divider light sx={{ mt: 2, mb: 2 }} />
      <Grid
        container
        justifyContent="space-between"
        alignItems="flex-start"
        spacing={1}
      >
        <Grid item xs={6}>
          <OHLCDetails title="Open" subtitle="1850.00" />
          <OHLCDetails title="Low" subtitle="1650.00" />
          <OHLCDetails title="Volume" subtitle="8.725" />
          <OHLCDetails title="LTQ" subtitle="25" />
          <OHLCDetails title="Expiry" subtitle="2021-12-30" />
          <OHLCDetails title="Lower Circuit" subtitle="0.05" />
          <OHLCDetails title="NIFTY BANK" subtitle="35065.60" />
        </Grid>
        <Grid item xs={6}>
          <OHLCDetails title="High" subtitle="1937.40" />
          <OHLCDetails title="Prev. close" subtitle="1816.45" />
          <OHLCDetails title="Avg. price" subtitle="1781.90" />
          <OHLCDetails title="LTT" subtitle="2021-12-30 15:29:14" />
          <OHLCDetails title="OI" subtitle="7500" />
          <OHLCDetails title="Upper circuit" subtitle="4104.00" />
        </Grid>
      </Grid>
    </>
  );
};

export default MarketDepthWidget;
