import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import React from "react";

type OHLCDetailsProps = {
  title: string;
  subtitle: string;
};

const OHLCDetails: React.FC<OHLCDetailsProps> = ({ title, subtitle }) => {
  return (
    <Stack justifyContent="space-between" direction="row">
      <Typography sx={{ fontSize: "12px" }} variant="subtitle1" color="#a0a0a0">
        {title}
      </Typography>
      <Typography sx={{ fontSize: "12px" }} variant="body1">
        {subtitle}
      </Typography>
    </Stack>
  );
};

export default OHLCDetails;
