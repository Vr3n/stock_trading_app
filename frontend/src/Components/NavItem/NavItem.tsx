import React from "react";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import { NavLink } from "react-router-dom";
import { SvgIconComponent } from "@mui/icons-material";
import { styled } from "@mui/material/styles";

type NavItemProps = {
  href: string;
  title: string;
  icon?: SvgIconComponent | null;
};

const StyledNavLink = styled(NavLink)(({ theme }) => ({
  textDecoration: "none",
  display: "inline-block",
  position: "relative",
  transition: "all 300ms",
  marginLeft: theme.spacing(4),
  color: "#6a6262",
  "&:hover": {
    color: "#000",
  },
  "&.active": {
    color: theme.palette.secondary.dark,
    fontWeight: theme.typography.fontWeightBold,
  },
  "&.active:after": {
    content: "''",
    position: "absolute",
    width: "100%",
    transform: "scaleX(1)",
    height: "0.125em",
    bottom: 0,
    left: 0,
    backgroundColor: theme.palette.secondary.dark,
    transformOrigin: "bottom right",
    transition: "transform 0.25s ease-out",
  },
  "&.active:focus:after": {
    content: "''",
    position: "absolute",
    width: "100%",
    transform: "scaleX(1)",
    height: "0.125em",
    bottom: 0,
    left: 0,
    backgroundColor: theme.palette.secondary.dark,
    transformOrigin: "bottom right",
    transition: "transform 0.25s ease-out",
  },
  "&.active:hover:after": {
    backgroundColor: theme.palette.secondary.dark,
  },
  "&:after": {
    content: "''",
    position: "absolute",
    width: "100%",
    transform: "scaleX(0)",
    height: "0.125em",
    bottom: 0,
    left: 0,
    backgroundColor: "#000",
    transformOrigin: "bottom right",
    transition: "transform 0.25s ease-out",
  },
  "&:hover:after": {
    transform: "scaleX(1)",
    transformOrigin: "bottom left",
  },
  "&:focus:after": {
    transform: "scaleX(1)",
    transformOrigin: "bottom left",
  },
  "&:focus": {
    outline: "none",
    color: theme.palette.secondary.dark,
  },
}));

const NavItem: React.FC<NavItemProps> = ({ href, title, icon = null }) => {
  return (
    <StyledNavLink activeClassName="active" exact to={href}>
      {icon && <IconButton color="inherit">{icon}</IconButton>}
      <Typography variant="body1">{title}</Typography>
    </StyledNavLink>
  );
};

export default NavItem;
