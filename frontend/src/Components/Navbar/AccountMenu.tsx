import React, { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Divider from "@mui/material/Divider";
import ListItemIcon from "@mui/material/ListItemIcon";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Logout from "@mui/icons-material/Logout";
import { useMutation, useQueryClient } from "react-query";
import { logoutAPI } from "../../api/logout";
import { useHistory } from "react-router-dom";
import { toast } from "react-toastify";

type AccountMenuProps = {
  anchorEl: null | HTMLElement;
  open: boolean;
  onClose: any;
  onClick?: any;
};

const AccountMenu: React.FC<AccountMenuProps> = ({
  anchorEl,
  open,
  onClose,
  onClick,
}) => {
  const query = useQueryClient();
  let history = useHistory();

  let [deleteOpen, setDeleteOpen] = useState(false);

  const userMutate = useMutation(logoutAPI, {
    onSuccess: () => {
      query.invalidateQueries("user");
      history.push("/accounts/login/");
      toast.success("Logged out successfully");
    },
  });

  const onDeleteClose = () => setDeleteOpen(false);
  const onDeleteOpen = () => setDeleteOpen(true);

  return (
    <>
      <Dialog
        open={deleteOpen}
        onClose={onDeleteClose}
        aria-labelledby="alert-delete-title"
        aria-describedby="alert-delete-description"
      >
        <DialogTitle id="alert-delete-title">Logout</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-delete-description">
            Are you sure you want to Logout ?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} variant="outlined" color="primary">
            Disagree
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => userMutate.mutate()}
            autoFocus
          >
            Logout
          </Button>
        </DialogActions>
      </Dialog>
      <Menu
        anchorEl={anchorEl}
        open={open}
        onClose={onClose}
        onClick={onClick}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: "''",
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{
          horizontal: "right",
          vertical: "top",
        }}
        anchorOrigin={{
          horizontal: "right",
          vertical: "bottom",
        }}
      >
        <MenuItem
          onClick={() => {
            history.push("/trading/profile/");
          }}
        >
          Profile
        </MenuItem>
        <Divider />
        <MenuItem onClick={onDeleteOpen}>
          <ListItemIcon>
            <Logout fontSize="small" />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </>
  );
};

export default AccountMenu;
