import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import ButtonBase from "@mui/material/ButtonBase";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import GCSLogo from "Imgs/gcs_logo@0.5x.png";
import { styled } from "@mui/material/styles";

import NavItem from "../NavItem/NavItem";
import LetterAvatar from "../LetterAvatar/LetterAvatar";
import AccountMenu from "./AccountMenu";
import { useUserInfoQuery } from "../../hooks/useUserInfo";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

type NavbarProps = {
  url: string;
};

const Navbar: React.FC<NavbarProps> = ({ url }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const { data: userData } = useUserInfoQuery();

  const open = Boolean(anchorEl);

  const handleProfileMenuClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleProfileMenuClose = () => {
    setAnchorEl(null);
  };

  return (
    <AppBar
      position="fixed"
      sx={{
        zIndex: (theme) => theme.zIndex.drawer + 1,
        backgroundColor: "rgb(255, 255, 255)",
      }}
    >
      <Toolbar>
        <Stack
          direction="row"
          spacing={1}
          divider={<Divider orientation="vertical" flexItem />}
        >
          <ButtonBase
            sx={{
              width: 80,
              height: 20,
            }}
          >
            <Img src={GCSLogo} alt="Greencurve Logo" />
          </ButtonBase>
          <Stack direction="row" spacing={1}>
            <Typography variant="body1" sx={{ color: "#000" }}>
              NIFTY 50: 325.00
            </Typography>
            <Typography variant="body1" sx={{ color: "#000" }}>
              BANK NIFTY: 525.00
            </Typography>
          </Stack>
        </Stack>
        <Box sx={{ flexGrow: 1 }} />
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
          }}
        >
          <NavItem href={`${url}/`} title="Home" />
          <NavItem href={`${url}/orders/`} title="Orders" />
          <NavItem href={`${url}/positions/`} title="Positions" />
          <NavItem href={`${url}/funds/`} title="Funds" />
          <IconButton
            size="small"
            edge="end"
            aria-label="account of current user"
            aria-haspopup="true"
            onClick={handleProfileMenuClick}
            color="inherit"
            sx={{
              marginLeft: 2,
            }}
          >
            {userData !== undefined && (
              <LetterAvatar
                size="small"
                name={`${userData.first_name} ${userData.last_name}`}
              />
            )}
          </IconButton>
          <AccountMenu
            anchorEl={anchorEl}
            open={open}
            onClose={handleProfileMenuClose}
          />
        </Box>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
