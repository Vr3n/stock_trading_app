import React, { useEffect, useState } from "react";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormGroup from "@mui/material/FormGroup";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import DraggablePaperComponent from "../DraggablePaperComponent/DraggablePaperComponent";
import OrderDialogSwitch from "./OrderDialogSwitch";
import OrderDialogTabs from "./OrderDialogTabs";
import { blue, orange } from "@mui/material/colors";
import FormInput from "../FormInput/FormInput";
import ResponsiveForm from "../../Forms/ResponsiveForm";
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import * as Yup from "yup";
import { useMutation, useQueryClient } from "react-query";
import { updateOrder } from "../../api/updateOrder";
import { toast } from "react-toastify";
import { useUserInfoQuery } from "../../hooks/useUserInfo";
import FormLabel from "@mui/material/FormLabel/FormLabel";
import Divider from "@mui/material/Divider";

type ModifyOrderDialogProps = {
  orderId: string;
  orderState: "buy" | "sell";
  open: boolean;
  handleClose: any;
  code: string | null;
  price: string;
  stockId?: any;
  stop_loss: any;
  quantity: string;
  margin: any;
  leverage: any;
  product: any;
  order_type: any;
};

interface ModifyOrderFormInput {
  stock: any;
  quantity: number;
  price: number;
  stop_loss: number;
  order_type: any;
  action: any;
  status: any;
  id: any;
  margin: any;
  leverage: any;
  product: any;
}

const schema = Yup.object({
  quantity: Yup.number().min(0),
  price: Yup.number().required("Price is required"),
  stop_loss: Yup.number().required("Stop loss is required"),
  order_type: Yup.string().required("Select Order type mandatory"),
});

const ModifyOrderDialog: React.FC<ModifyOrderDialogProps> = ({
  orderState = "",
  open = false,
  handleClose,
  code,
  price = "0",
  stockId,
  quantity,
  orderId,
  stop_loss,
  margin,
  leverage = "2x",
  product,
  order_type,
}) => {
  const [toggleOrderState, setToggleOrderState] = useState<string>(orderState);

  const query = useQueryClient();

  const {
    data: userInfoData,
    isLoading: isUserLoading,
    isError: userError,
  } = useUserInfoQuery();

  const updateOrderMutation = useMutation(updateOrder, {
    onSuccess: () => { 
      query.invalidateQueries("orders");
      query.invalidateQueries('user_funds');
    },
  });

  const onSubmit = async (data: any) => {
    console.log(data);
    updateOrderMutation.mutate(
      {
        ...data,
        id: orderId,
        stock: code,
        user: userInfoData.id,
        action: toggleOrderState.toUpperCase(),
        status: "Pending",
      },
      {
        onSuccess: () => {
          toast.success("Order successfully updated!");
          handleClose();
        },
        onError: (err: any) => {
          toast.error(err.message);
        },
      }
    );
  };

  const { control, handleSubmit, watch, setValue } =
    useForm<ModifyOrderFormInput>({
      resolver: yupResolver(schema),
      mode: "onChange",
      defaultValues: {
        order_type: order_type,
        stock: code,
        id: orderId,
        margin: margin,
        leverage: leverage,
        product: product,
      },
    });

  const watchQuantity = watch("quantity");
  const watchOrderType = watch("order_type");
  const watchPrice = watch("price");
  const watchLeverage = watch("leverage");

  useEffect(() => {
    setValue("order_type", order_type);
    setValue("stock", code);
    setValue("product", product);
    setValue("quantity", parseInt(quantity));
    setValue("price", parseFloat(price));
    setValue("margin", margin);
    setValue("leverage", leverage || "2x");
    setValue("stop_loss", parseFloat(stop_loss));
  }, [
    orderState,
    open,
    handleClose,
    code,
    price,
    stockId,
    quantity,
    stop_loss,
    margin,
    leverage,
    product,
    order_type,
  ]);

  useEffect(() => {
    setValue(
      "margin",
      parseFloat(
        (
          (watchQuantity * watchPrice) /
          parseInt(watchLeverage.split("")[0])
        ).toString()
      ).toFixed(2)
    );
  }, [watchPrice, watchQuantity]);

  useEffect(() => {
    if (watchPrice == 0) {
      setValue("order_type", "Market");
    }
  }, [watchPrice]);

  const handleOrderStateToggle = () => {
    if (toggleOrderState === "buy") {
      setToggleOrderState("sell");
    }
    if (toggleOrderState === "sell") {
      setToggleOrderState("buy");
    }
  };

  const theme = React.useMemo(() => {
    if (toggleOrderState === "sell") {
      return createTheme({
        palette: {
          primary: {
            light: orange[300],
            main: orange[500],
            dark: orange[900],
          },
        },
      });
    }
    return createTheme({
      palette: {
        primary: {
          light: blue[300],
          main: blue[500],
          dark: blue[700],
        },
      },
    });
  }, [toggleOrderState]);

  useEffect(() => {
    setToggleOrderState(orderState);
  }, [orderState]);

  return (
    <ThemeProvider theme={theme}>
      <Dialog
        open={open}
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            handleClose();
          }
        }}
        PaperComponent={DraggablePaperComponent}
        aria-labelledby="draggable-order-dialog-title"
        disableEnforceFocus
        disableScrollLock
        hideBackdrop
        maxWidth="md"
        fullWidth
        sx={{
          position: "fixed",
          height: "fit-content",
          width: "fit-content",
        }}
      >
        <DialogTitle
          sx={{
            cursor: "move",
            background: (theme) => theme.palette.primary.main,
            color: "white",
          }}
          id="draggable-order-dialog-title"
        >
          <Stack
            direction="row"
            sx={{
              justifyContent: "space-between",
            }}
            spacing={5}
          >
            <Typography variant="h6">
              {toggleOrderState === "buy" && "Buy"}
              {toggleOrderState === "sell" && "Sell"} {code}&nbsp;
              <span style={{ fontSize: "0.75em" }}>x</span> {watchQuantity}
              &nbsp; QTY
            </Typography>
            <OrderDialogSwitch
              checked={toggleOrderState === "sell"}
              onClick={handleOrderStateToggle}
            />
          </Stack>
        </DialogTitle>
        <DialogContent
          sx={{
            m: 0,
            p: 0,
          }}
        >
          <Stack spacing={3}>
            <OrderDialogTabs />
            <form
              style={{
                paddingRight: "1.25em",
                paddingLeft: "1.25em",
              }}
            >
              <FormGroup
                sx={{
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  name="product"
                  defaultValue={product}
                  render={({ field }) => (
                    <>
                      <FormLabel id="product_selection">Product</FormLabel>
                      <RadioGroup
                        row
                        {...field}
                        aria-label="product_type_selection"
                      >
                        <FormControlLabel
                          value="Intraday"
                          control={<Radio />}
                          label="Intraday"
                        />
                        <FormControlLabel
                          value="Carry Forward"
                          control={<Radio />}
                          label="Carry Forward"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  gap: "0.625rem",
                  pb: (theme) => theme.spacing(2),
                  pt: (theme) => theme.spacing(2),
                }}
                row
              >
                <FormInput
                  required
                  control={control}
                  name="quantity"
                  label="Qty"
                  type="number"
                  defaultValue={quantity}
                  fullWidth={false}
                  inputProps={{
                    min: 0,
                  }}
                />
                <FormInput
                  required
                  control={control}
                  disabled={watchOrderType === "Market"}
                  name="price"
                  label="Price"
                  fullWidth={false}
                  type="number"
                  defaultValue={parseInt(price)}
                  inputProps={{
                    min: 0,
                  }}
                />
                <FormInput
                  required
                  control={control}
                  disabled={watchOrderType === "Market"}
                  name="stop_loss"
                  label="Stop Loss"
                  fullWidth={false}
                  type="number"
                  defaultValue={parseInt(stop_loss)}
                  inputProps={{
                    min: 0,
                  }}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  mt: (theme) => theme.spacing(1),
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  defaultValue={leverage}
                  name="leverage"
                  render={({ field }) => (
                    <>
                      <FormLabel id="leverage_form_label">Leverage</FormLabel>
                      <RadioGroup
                        row
                        {...field}
                        aria-label="order type selection"
                      >
                        <FormControlLabel
                          value="1x"
                          control={<Radio />}
                          label="1x"
                        />
                        <FormControlLabel
                          value="2x"
                          control={<Radio />}
                          label="2x"
                        />
                        <FormControlLabel
                          value="3x"
                          control={<Radio />}
                          label="3x"
                        />
                        <FormControlLabel
                          value="4x"
                          control={<Radio />}
                          label="4x"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  mt: (theme) => theme.spacing(1),
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  name="order_type"
                  defaultValue={order_type}
                  render={({ field }) => (
                    <>
                      <FormLabel id="leverage_form_label">Order Type</FormLabel>
                      <RadioGroup
                        row
                        {...field}
                        aria-label="order type selection"
                      >
                        <FormControlLabel
                          value="Bracket"
                          control={<Radio />}
                          label="Bracket"
                        />
                        <FormControlLabel
                          value="Market"
                          control={<Radio />}
                          label="Market"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <Stack
                direction="row"
                spacing={2}
                divider={<Divider orientation="vertical" flexItem />}
              >
                <Typography variant="body1">
                  <b>
                    Margin:{" "}
                    <Typography
                      sx={{ color: (theme) => theme.palette.primary.main }}
                      variant="body1"
                      component="span"
                    >
                      {parseFloat(
                        (
                          (watchQuantity * watchPrice) /
                          parseInt(watchLeverage.split("")[0])
                        ).toString()
                      ).toFixed(2)}
                    </Typography>
                  </b>
                </Typography>
                <Typography variant="body1">
                  <b>
                    Available Margin:{" "}
                    <Typography
                      sx={{ color: (theme) => theme.palette.primary.main }}
                      variant="body1"
                      component="span"
                    >
                      {isUserLoading
                        ? "Loading..."
                        : parseFloat(
                            (
                              parseFloat(userInfoData?.available_margin) -
                              parseFloat(
                                (
                                  (watchQuantity * watchPrice) /
                                  parseInt(watchLeverage.split("")[0])
                                ).toString()
                              )
                            ).toString()
                          ).toFixed(2)}
                    </Typography>
                  </b>
                </Typography>
              </Stack>
            </form>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleSubmit(onSubmit)} variant="contained">
            <Typography variant="body1" color="white">
              {toggleOrderState === "buy" && "Buy"}
              {toggleOrderState === "sell" && "Sell"}
            </Typography>
          </Button>
          <Button onClick={handleClose} variant="outlined">
            <Typography variant="body1">CANCEL</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    </ThemeProvider>
  );
};

export default ModifyOrderDialog;
