import React, { useState, useEffect } from "react";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormGroup from "@mui/material/FormGroup";
import Stack from "@mui/material/Stack";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import DraggablePaperComponent from "../DraggablePaperComponent/DraggablePaperComponent";
import OrderDialogSwitch from "./OrderDialogSwitch";
import OrderDialogTabs from "./OrderDialogTabs";
import { blue, orange } from "@mui/material/colors";
import FormInput from "../FormInput/FormInput";
import ResponsiveForm from "../../Forms/ResponsiveForm";
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import * as Yup from "yup";
import { Query, useMutation, useQueryClient } from "react-query";
import { createOrder } from "../../api/createOrder";
import { toast } from "react-toastify";
import { useUserInfoQuery } from "../../hooks/useUserInfo";
import FormLabel from "@mui/material/FormLabel/FormLabel";
import { useUserFundQuery } from "../../hooks/useUserFundsQuery";

type OrderDialogProps = {
  orderState: "buy" | "sell";
  open: boolean;
  handleClose: any;
  code: string;
  price: string;
  stockId: any;
  product: any;
};

interface OrderFormInput {
  stock: any;
  quantity: number;
  price: number;
  stop_loss: number;
  order_type: any;
  action: any;
  status: any;
  margin: any;
  leverage: any;
  product: any;
}

const schema = Yup.object({
  quantity: Yup.number().min(0),
  price: Yup.number().required("Price is required"),
  stop_loss: Yup.number().required("Stop loss is required"),
  order_type: Yup.string().required("Select Order type mandatory"),
});

const OrderDialog: React.FC<OrderDialogProps> = ({
  orderState = "",
  open = false,
  handleClose,
  code,
  price,
  stockId,
}) => {
  const [toggleOrderState, setToggleOrderState] = useState<string>(orderState);

  const { control, handleSubmit, watch, setValue } = useForm<OrderFormInput>({
    resolver: yupResolver(schema),
    mode: "onChange",
    defaultValues: {
      quantity: 1,
      order_type: "Bracket",
      margin: (parseFloat(price) * 1) / 2,
      leverage: "2x",
      product: "Intraday",
    },
  });

  useEffect(() => {
    setValue("price", parseFloat(price));
    setValue("stop_loss", parseFloat(price));
    setValue(
      "margin",
      parseFloat(((parseFloat(price) * 1) / 2).toString()).toFixed(2)
    );
  }, [orderState, open, handleClose, code, price, stockId]);

  const watchQuantity = watch("quantity");

  const watchOrderType = watch("order_type");

  const watchLeverage = watch("leverage");

  const watchPrice = watch("price", parseFloat(price));

  // loading the user funds.
  // const {
  //   data: userFundsData,
  //   isLoading: isUserFundsLoading,
  //   isError: isUserFundsError,
  //   error: userFundsError,
  // } = useUserFundQuery((data: any) => data.available_margin);

  const {
    data: userInfoData,
    isLoading: isUserLoading,
    isError: isUserError,
    error: userInfoError,
  } = useUserInfoQuery();

  // Seting the value of the margin.
  // based on the values of quantity, price & leverage.
  useEffect(() => {
    setValue(
      "margin",
      parseFloat(
        (
          (watchQuantity * watchPrice) /
          parseInt(watchLeverage.split("")[0])
        ).toString()
      ).toFixed(2)
    );
  }, [watchPrice, watchQuantity]);

  // useEffect(() => {
  //   if (userFundsData) {
  //     console.log(userFundsData);
  //   }
  // }, [userFundsData]);

  // Watching the Price of order.
  // Market order has price of 0.
  // Bracket order has price more than 1.
  useEffect(() => {
    if (watchPrice == 0) {
      setValue("order_type", "Market");
    }
  }, [watchPrice]);

  // Watching the Leverage
  // 1x leverage means product is Carry Forward.
  // Other leverages means It is Intraday.
  useEffect(() => {
    if (watchLeverage === "1x") {
      setValue("product", "Carry Forward");
    } else {
      setValue("product", "Intraday");
    }
  }, [watchLeverage]);

  const handleOrderStateToggle = () => {
    if (toggleOrderState === "buy") {
      setToggleOrderState("sell");
    }
    if (toggleOrderState === "sell") {
      setToggleOrderState("buy");
    }
  };

  const theme = React.useMemo(() => {
    if (toggleOrderState === "sell") {
      return createTheme({
        palette: {
          primary: {
            light: orange[300],
            main: orange[500],
            dark: orange[900],
          },
        },
      });
    }
    return createTheme({
      palette: {
        primary: {
          light: blue[300],
          main: blue[500],
          dark: blue[700],
        },
      },
    });
  }, [toggleOrderState]);

  useEffect(() => {
    setToggleOrderState(orderState);
  }, [orderState]);

  const query = useQueryClient();

  const createOrderMutation = useMutation(createOrder, {
    onSuccess: () => { 
      query.invalidateQueries("orders");
      query.invalidateQueries('user_funds');
    },
  });

  const onSubmit = async (data: any) => {
    createOrderMutation.mutate(
      {
        ...data,
        stock: code,
        action: toggleOrderState.toUpperCase(),
        user: userInfoData.id,
        status: "Pending",
      },
      {
        onSuccess: () => {
          toast.success("Order sucessfully created!");
          handleClose();
        },
        onError: (err: any) => {
          toast.error(err.message);
        },
      }
    );
  };

  return (
    <ThemeProvider theme={theme}>
      <Dialog
        open={open}
        onClose={(event, reason) => {
          if (reason !== "backdropClick") {
            handleClose();
          }
        }}
        PaperComponent={DraggablePaperComponent}
        aria-labelledby="draggable-order-dialog-title"
        disableEnforceFocus
        disableScrollLock
        hideBackdrop
        maxWidth="md"
        fullWidth
        sx={{
          position: "fixed",
          height: "fit-content",
          width: "fit-content",
        }}
      >
        <DialogTitle
          sx={{
            cursor: "move",
            background: (theme) => theme.palette.primary.main,
            color: "white",
          }}
          id="draggable-order-dialog-title"
        >
          <Stack
            direction="row"
            sx={{
              justifyContent: "space-between",
            }}
            spacing={5}
          >
            <Typography variant="h6">
              {toggleOrderState === "buy" && "Buy"}
              {toggleOrderState === "sell" && "Sell"} {code}&nbsp;
              <span style={{ fontSize: "0.75em" }}>x</span> {watchQuantity}
              &nbsp; QTY
            </Typography>
            <OrderDialogSwitch
              checked={toggleOrderState === "sell"}
              onClick={handleOrderStateToggle}
            />
          </Stack>
        </DialogTitle>
        <DialogContent
          sx={{
            m: 0,
            p: 0,
          }}
        >
          <Stack spacing={3}>
            <OrderDialogTabs />
            <form
              style={{
                paddingRight: "1.25em",
                paddingLeft: "1.25em",
              }}
            >
              <FormGroup
                sx={{
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  name="product"
                  render={({ field }) => (
                    <>
                      <FormLabel id="product_selection">Product</FormLabel>
                      <RadioGroup
                        row
                        defaultValue="Intraday"
                        {...field}
                        aria-label="product_type_selection"
                      >
                        <FormControlLabel
                          value="Intraday"
                          disabled={watchLeverage === "1x"}
                          control={<Radio />}
                          label="Intraday"
                        />
                        <FormControlLabel
                          value="Carry Forward"
                          disabled={watchLeverage !== "1x"}
                          control={<Radio />}
                          label="Carry Forward"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  gap: "0.625rem",
                  pb: (theme) => theme.spacing(2),
                  pt: (theme) => theme.spacing(2),
                }}
                row
              >
                <FormInput
                  required
                  control={control}
                  name="quantity"
                  label="Qty"
                  type="number"
                  defaultValue={1}
                  fullWidth={false}
                  inputProps={{
                    min: 0,
                  }}
                />
                <FormInput
                  required
                  control={control}
                  name="price"
                  disabled={watchOrderType === "Market"}
                  label="Price"
                  fullWidth={false}
                  type="number"
                  defaultValue={price}
                  inputProps={{
                    min: 0,
                  }}
                />
                <FormInput
                  required
                  control={control}
                  disabled={watchOrderType === "Market"}
                  name="stop_loss"
                  label="Stop Loss"
                  fullWidth={false}
                  type="number"
                  defaultValue={price}
                  inputProps={{
                    min: 0,
                  }}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  mt: (theme) => theme.spacing(1),
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  name="leverage"
                  render={({ field }) => (
                    <>
                      <FormLabel id="leverage_form_label">Leverage</FormLabel>
                      <RadioGroup
                        row
                        {...field}
                        aria-label="order type selection"
                      >
                        <FormControlLabel
                          value="1x"
                          control={<Radio />}
                          label="1x"
                        />
                        <FormControlLabel
                          value="2x"
                          control={<Radio />}
                          label="2x"
                        />
                        <FormControlLabel
                          value="3x"
                          control={<Radio />}
                          label="3x"
                        />
                        <FormControlLabel
                          value="4x"
                          control={<Radio />}
                          label="4x"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <hr />
              <FormGroup
                sx={{
                  mt: (theme) => theme.spacing(1),
                  mb: (theme) => theme.spacing(1),
                }}
              >
                <Controller
                  control={control}
                  name="order_type"
                  render={({ field }) => (
                    <>
                      <FormLabel id="leverage_form_label">Order Type</FormLabel>
                      <RadioGroup
                        row
                        defaultValue="Bracket"
                        {...field}
                        aria-label="order type selection"
                      >
                        <FormControlLabel
                          value="Bracket"
                          control={<Radio />}
                          label="Bracket"
                        />
                        <FormControlLabel
                          value="Market"
                          control={<Radio />}
                          label="Market"
                        />
                      </RadioGroup>
                    </>
                  )}
                />
              </FormGroup>
              <Stack
                direction="row"
                spacing={2}
                divider={<Divider orientation="vertical" flexItem />}
              >
                <Typography variant="body1">
                  <b>
                    Margin:{" "}
                    <Typography
                      sx={{ color: (theme) => theme.palette.primary.main }}
                      variant="body1"
                      component="span"
                    >
                      {parseFloat(
                        (
                          (watchQuantity * watchPrice) /
                          parseInt(watchLeverage.split("")[0])
                        ).toString()
                      ).toFixed(2)}
                    </Typography>
                  </b>
                </Typography>
                <Typography variant="body1">
                  <b>
                    Available Margin:{" "}
                    <Typography
                      sx={{ color: (theme) => theme.palette.primary.main }}
                      variant="body1"
                      component="span"
                    >
                      {isUserLoading
                        ? "Loading..."
                        : parseFloat(
                            (
                              parseFloat(userInfoData?.available_margin) -
                              parseFloat(
                                (
                                  (watchQuantity * watchPrice) /
                                  parseInt(watchLeverage.split("")[0])
                                ).toString()
                              )
                            ).toString()
                          ).toFixed(2)}
                    </Typography>
                  </b>
                </Typography>
              </Stack>
            </form>
          </Stack>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleSubmit(onSubmit)} variant="contained">
            <Typography variant="body1" color="white">
              {toggleOrderState === "buy" && "Buy"}
              {toggleOrderState === "sell" && "Sell"}
            </Typography>
          </Button>
          <Button onClick={handleClose} variant="outlined">
            <Typography variant="body1">CANCEL</Typography>
          </Button>
        </DialogActions>
      </Dialog>
    </ThemeProvider>
  );
};

export default OrderDialog;
