import React from "react";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { grey } from "@mui/material/colors";

const OrderDialogTabs = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) =>
    setValue(newValue);

  return (
    <Tabs
      value={value}
      aria-label="order type selection"
      onChange={handleChange}
      selectionFollowsFocus
      sx={{
        backgroundColor: grey[100],
      }}
    >
      <Tab label="Cover" />
    </Tabs>
  );
};

export default OrderDialogTabs;
