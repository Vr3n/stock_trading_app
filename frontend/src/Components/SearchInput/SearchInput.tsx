import React, { useState, useEffect } from "react";
import Autocomplete from "@mui/material/Autocomplete";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import SearchIcon from "@mui/icons-material/Search";
import { fetchStockCodes } from "../../api/fetchStockCodes";
import { toast } from "react-toastify";
import match from "autosuggest-highlight/match";
import parse from "autosuggest-highlight/parse";
import { useQuery, useMutation, useQueryClient } from "react-query";
import { useWatchlistQuery } from "../../hooks/useWatchlistQuery";
import { updateWatchList } from "../../api/updateWatchList";

import { useUserInfoQuery } from "../../hooks/useUserInfo";

interface SearchInputOptions {
  id: string;
  name: string;
  code: string;
}

type SearchInputProps = {
  props: any;
};

const SearchInput: React.FC<SearchInputProps> = (props) => {
  const query = useQueryClient();

  const [searchData, setSearcData] = useState([]);
  const [value, setValue] = useState<string | null>(null);
  const [inputValue, setInputValue] = useState("");

  let { data, isLoading } = useQuery("stock_list", fetchStockCodes, {
    refetchOnWindowFocus: false,
  });

  useEffect(() => {
    if (data !== undefined) {
      setSearcData(data);
    }
  }, [data]);

  return (
    <Autocomplete
      id="watchlist_autocomplete"
      popupIcon={<SearchIcon />}
      size="small"
      loading={isLoading}
      renderInput={(params) => (
        <TextField
          {...params}
          sx={{ fontSize: "12px", borderRadius: "0" }}
          placeholder="Add stocks to Watchlist"
        />
      )}
      options={searchData}
      // @ts-ignore
      value={props.value}
      // @ts-ignore
      onChange={props.onChange}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      getOptionLabel={(option: SearchInputOptions) =>
        `${option.code}: ${option.name}`
      }
      renderOption={(props, option, { inputValue }) => {
        const matches = match(option.name || option.code, inputValue);
        const parts = parse(option.name || option.code, matches);

        return (
          <li {...props} style={{ fontSize: "12px" }}>
            <div>
              {parts.map((part: any, index: any) => (
                <span
                  key={index}
                  style={{
                    fontWeight: part.highlight ? 700 : 400,
                  }}
                >
                  {part.text}
                </span>
              ))}
            </div>
          </li>
        );
      }}
    />
  );
};

export default SearchInput;
