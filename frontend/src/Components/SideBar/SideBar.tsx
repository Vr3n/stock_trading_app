import React from "react";
import Drawer from "@mui/material/Drawer";
import Toolbar from "@mui/material/Toolbar";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import Divider from "@mui/material/Divider";
import ListItemText from "@mui/material/ListItemText";
import MenuItem from "@mui/material/MenuItem";
import MenuList from "@mui/material/MenuList";
import Stack from "@mui/material/Stack";
import StockSearchInput from "../SearchInput/StockSearchInput";
import WatchlistCard from "../WatchlistCard/WatchlistCard";

const DRAWER_WIDTH = 360;

const SideBar = () => {
  return (
    <Drawer
      variant="permanent"
      sx={{
        width: DRAWER_WIDTH,
        flexShrink: 0,
        [`& .MuiDrawer-paper`]: {
          width: DRAWER_WIDTH,
          boxSizing: "border-box",
        },
      }}
      PaperProps={{
        style: {
          position: "absolute",
          minHeight: "100vh",
        },
        elevation: 2,
      }}
    >
      <Toolbar />
      <Stack
        sx={{
          marginTop: "0.938em",
        }}
      >
        <div
          style={{
            marginBottom: "0.938em",
          }}
        >
          <StockSearchInput />
        </div>
        <Divider />
        <WatchlistCard code="INFY" percent1="10.36%" price="13.40" />
        <WatchlistCard code="RELIANCE" percent1="-0.25%" price="2379.90" />
        <WatchlistCard code="PARLEIND" percent1="1.69%" price="8.44" />
        <WatchlistCard code="APPLE" percent1="1000.00%" price="242019.0" />
      </Stack>
    </Drawer>
  );
};

export default SideBar;
