import React, { useEffect, useState } from "react";

import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { useUserBankQuery } from "../../hooks/useUserBankQuery";
import { Link } from "react-router-dom";
import { Button, LinearProgress } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import AddBankDialog from "../AddBankDialog/AddBankDialog";

interface UserBanksProps {
  user_id: any;
}

const BankUpdateDialogDefaults = {
  bank_name: "",
  account_number: "",
  ifsc_code: "",
  id: "",
  bank_id: "",
  update: false,
  open: false,
};

const UserBanks: React.FC<UserBanksProps> = ({ user_id }) => {
  const {
    data: userBankData,
    isLoading: isUserBankLoading,
    isError: isUserBankError,
    error: userBankError,
  } = useUserBankQuery((data: any) => data);

  const [open, setOpen] = useState(BankUpdateDialogDefaults);

  const handleDialogOpen = ({
    bank_name,
    account_number,
    ifsc_code,
    update,
    bank_id = "",
    id = "",
  }: any) => {
    setOpen({
      bank_name,
      account_number,
      ifsc_code,
      update,
      id,
      bank_id,
      open: true,
    });
  };

  const handleDialogClose = () => setOpen({ ...BankUpdateDialogDefaults });

  useEffect(() => {
    if (userBankData !== undefined) {
      console.log(userBankData);
    }
  }, [userBankData]);

  return isUserBankLoading ? (
    <LinearProgress />
  ) : (
    <Paper
      elevation={2}
      sx={{
        p: 5,
      }}
    >
      <AddBankDialog
        user_id={user_id}
        open={open.open}
        update={open.update}
        bank_name={open.bank_name}
        account_number={open.account_number}
        ifsc_code={open.ifsc_code}
        bank_id={open.bank_id}
        handleClose={handleDialogClose}
      />
      <Stack spacing={2} divider={<Divider />}>
        <Typography gutterBottom variant="h5">
          Bank Details
        </Typography>
        {userBankData.length === 0 ? (
          <Paper elevation={2} sx={{ p: 2 }}>
            <Stack direction="row" spacing={2} justifyContent="space-between">
              <Typography variant="body1">
                You haven't added any bank accounts.
              </Typography>
            </Stack>
          </Paper>
        ) : (
          // @ts-ignore
          userBankData.map((bank, idx) => (
            <Paper elevation={2} sx={{ p: 2 }}>
              <Stack
                direction="row"
                spacing={2}
                key={bank.id}
                justifyContent="space-between"
                alignItems="flex-start"
              >
                <div>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    sx={{ paddingBottom: (theme) => theme.spacing(1) }}
                  >
                    <Typography variant="body1">Bank Name:</Typography>
                    <Typography variant="body1" sx={{ fontWeight: "bold" }}>
                      {bank.bank_name}
                    </Typography>
                  </Stack>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    sx={{ paddingBottom: (theme) => theme.spacing(1) }}
                  >
                    <Typography variant="body1">Account Number: </Typography>
                    <Typography variant="body1" sx={{ fontWeight: "bold" }}>
                      {bank.account_number}
                    </Typography>
                  </Stack>
                  <Stack
                    alignItems="flex-start"
                    direction="row"
                    sx={{ paddingBottom: (theme) => theme.spacing(1) }}
                  >
                    <Typography variant="body1">IFSC Code:</Typography>
                    <Typography variant="body1" sx={{ fontWeight: "bold" }}>
                      {bank.ifsc_code}
                    </Typography>
                  </Stack>
                </div>
                <div>
                  <Button
                    onClick={() => {
                      console.log(bank.id);
                      handleDialogOpen({
                        bank_name: bank.bank_name,
                        bank_id: bank.id,
                        account_number: bank.account_number,
                        ifsc_code: bank.ifsc_code,
                        update: true,
                      });
                    }}
                    variant="contained"
                    color="primary"
                    startIcon={<EditIcon />}
                  >
                    Edit
                  </Button>
                </div>
              </Stack>
            </Paper>
          ))
        )}
        <Button
          sx={{ mt: 2 }}
          component="button"
          color="primary"
          variant="contained"
          startIcon={<AddCircleOutlineIcon />}
          onClick={() => {
            handleDialogOpen({
              bank_name: "",
              account_number: "",
              ifsc_code: "",
              update: false,
            });
          }}
        >
          Add Bank Account
        </Button>
      </Stack>
    </Paper>
  );
};

export default UserBanks;
