import React, { useState, useEffect } from "react";
import { styled } from "@mui/material/styles";
import Paper from "@mui/material/Paper";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Stack from "@mui/material/Stack";
import ArrowUpwardIcon from "@mui/icons-material/ArrowUpward";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import Chip from "@mui/material/Chip";
import Collapse from "@mui/material/Collapse";
import MarketDepthWidget from "../MarketDepthWidget/MarketDepthWidget";

// import BuySellWidget from "../BuySellWidget/BuySellWidget";

const CircleUpIcon = styled(ArrowUpwardIcon)(({ theme }) => ({
  color: theme.palette.secondary.dark,
}));

const CircleDownIcon = styled(ArrowDownwardIcon)(({ theme }) => ({
  color: theme.palette.error.main,
}));

type WatchListCardProps = {
  code: string;
  percent1: string;
  price: string;
  onClick?: any;
  onDelete?: any;
  stockId: any;
};

const WatchlistCard: React.FC<WatchListCardProps> = ({
  code,
  percent1,
  price,
  stockId,
  onClick,
  onDelete,
}) => {
  const [showWidget, setShowWidget] = useState<boolean>(false);
  const [expandDepth, setExpandDepth] = useState<boolean>(false);

  const handleMouseEnter = () => setShowWidget(true);
  const handleMouseLeave = () => setShowWidget(false);

  const handleExpandToggle = () => {
    setExpandDepth((expand) => !expand);
  };

  return (
    <>
      <Paper
        variant="outlined"
        sx={{
          p: 1,
          position: "relative",
          cursor: "pointer",
          "&:hover": {
            backgroundColor: "rgba(241, 241, 242, 0.7)",
          },
        }}
        // onClick={onClick}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      >
        <Stack
          direction="row"
          sx={{
            justifyContent: "space-between",
          }}
          spacing={1}
        >
          <Typography
            sx={{ color: (theme) => theme.palette.secondary.dark }}
            variant="subtitle1"
          >
            {code}
          </Typography>
          {showWidget && (
            <Stack direction="row" spacing={1}>
              <Chip
                label="Buy"
                size="small"
                aria-label="Buy Button"
                color="primary"
                onClick={() =>
                  onClick({
                    code: code,
                    price: price,
                    orderAction: "buy",
                    stockId: stockId,
                  })
                }
              />
              <Chip
                label="Sell"
                size="small"
                aria-label="Sell Button"
                color="warning"
                onClick={() =>
                  onClick({
                    code: code,
                    price: price,
                    orderAction: "sell",
                    stockId: stockId,
                  })
                }
              />
              <Chip
                label="Depth"
                size="small"
                aria-label="Market Depth"
                color="success"
                onClick={handleExpandToggle}
              />
              <Chip
                label="Delete"
                size="small"
                aria-label="Delete Button"
                color="error"
                onClick={() => onDelete({ stockId, code })}
              />
            </Stack>
          )}
          <Stack
            direction="row"
            sx={{ justifyContent: "space-between", alignItems: "center" }}
            spacing={2}
          >
            <Typography
              variant="subtitle2"
              sx={{
                color: (theme) => theme.palette.secondary.dark,
              }}
            >
              {percent1}
            </Typography>
            <CircleUpIcon />
            <Typography
              variant="subtitle2"
              sx={{
                color: (theme) => theme.palette.secondary.dark,
              }}
            >
              {price}
            </Typography>
          </Stack>
        </Stack>
      </Paper>
      <Collapse in={expandDepth} timeout="auto" unmountOnExit>
        <Paper
          sx={{
            p: 1,
            alignItems: "center",
          }}
          elevation={2}
        >
          <MarketDepthWidget />
        </Paper>
      </Collapse>
    </>
  );
};

export default WatchlistCard;
