import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import Alert from "@mui/material/Alert";
import { Link } from "react-router-dom";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDocumentTitle } from "../../hooks/useDocumentTitle";
import { forgotPasswordAPI } from "../../api/forgotPassword";
import LoadingButton from "@mui/lab/LoadingButton";
import { useQueryClient, useMutation } from "react-query";
import { toast } from "react-toastify";
import ResponsiveForm from "../ResponsiveForm";
import FormInput from "../../Components/FormInput/FormInput";

interface ForgotPasswordInputs {
  email: string;
}

const schema = Yup.object().shape({
  email: Yup.string()
    .email("Enter valid email format")
    .required("Email is required!"),
});

const ForgotPasswordForm = () => {
  useDocumentTitle("Forgot Password");

  const [error, setError] = useState<any>(null);

  const { control, handleSubmit, formState } = useForm<ForgotPasswordInputs>({
    resolver: yupResolver(schema),
    mode: "onChange",
  });

  const query = useQueryClient();
  const userMutate = useMutation((data) => forgotPasswordAPI(data))

  const onSubmit: SubmitHandler<ForgotPasswordInputs> = async (data) => {
    // @ts-ignore
    userMutate.mutate(data, {
      onSuccess: () => {
        toast.success("Password Reset Email Sent Successfully!")
      },
      onError: (err: any) => {
        toast.error(err.message)
        setError(err.message)
      }
    })
  };

  return (
    <ResponsiveForm
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      noValidate
    >
      {error !== null && (
        <Alert
          sx={{ marginBottom: (theme) => theme.spacing(1) }}
          severity="error"
        >
          {error}
        </Alert>
      )}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormInput
            name="email"
            label="Email Address"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <LoadingButton
            type="submit"
            loading={userMutate.isLoading}
            loadingPosition="end"
            fullWidth
            variant="contained"
            color="success"
          >Reset Password</LoadingButton>
        </Grid>
        <Grid item xs={12}>
          <Link to="/accounts/login/">Back to login.</Link>
        </Grid>
      </Grid>
    </ResponsiveForm>
  );
};

export default ForgotPasswordForm;
