import React from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import { toast } from "react-toastify";
import { useDocumentTitle } from "../../hooks/useDocumentTitle";
import ResponsiveForm from "../ResponsiveForm";

const LogoutForm = () => {
  useDocumentTitle("Logout ");

  return (
    <ResponsiveForm component="form" noValidate>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Button type="submit" fullWidth variant="contained" color="success">
            Logout
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button type="submit" fullWidth variant="outlined" color="danger">
            Cancel
          </Button>
        </Grid>
      </Grid>
    </ResponsiveForm>
  );
};

export default LogoutForm;
