import React from 'react';
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import FormInput from "../../Components/FormInput/FormInput";
import * as Yup from "yup";
import { toast } from "react-toastify";
import LoadingButton from "@mui/lab/LoadingButton";
import { useMutation, useQueryClient } from "react-query";
import ResponsiveForm from "../ResponsiveForm";

// Schema for validating the user otp.
const schema = Yup.object().shape({
  otp: Yup.string().required("Otp is required")
    .matches(/^([^A-Za-z]*)$/, "OTP should contain only digits")
    .matches(
      /^([^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*)$/,
      "Should not contain special characters."
    )
    .min(6, "OTP length should be 6 digits")
    .max(6, "OTP length should not exceed more than 6. digits"),
})

interface OTPInput {
  otp: string;
}

const OtpForm = () => {
  const { handleSubmit, control, formState } = useForm({
    resolver: yupResolver(schema),
    mode: 'onChange'
  })

  const onSubmit: SubmitHandler<OTPInput> = async (data: OTPInput) => {
    console.log(data);
  }

  return (
    <>
      <Typography component="h2" variant="h6" gutterBottom>
        Enter the OTP sent to your mobile number
      </Typography>
      <ResponsiveForm onSubmit={handleSubmit(onSubmit)}>
        <FormInput control={control} required autoFocus label="OTP" name="otp" />
        <Stack direction="row">
          <LoadingButton
            type="submit"
            loading={false}
            loadingPosition="end"
            variant="contained"
            color="success"
          >
            Submit
          </LoadingButton>
          <LoadingButton
            type="submit"
            loading={false}
            loadingPosition="end"
            variant="outlined"
            color="warning"
          >
            Resend OTP
          </LoadingButton>
        </Stack>
      </ResponsiveForm>
    </>
  );
};

export default OtpForm;
