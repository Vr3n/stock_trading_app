import React, { useState, useEffect } from "react";
import { Link, useHistory, useParams } from "react-router-dom";
import Grid from "@mui/material/Grid";
import { toast } from "react-toastify";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDocumentTitle } from "../../hooks/useDocumentTitle";
import ResponsiveForm from "../ResponsiveForm";
import FormInput from "../../Components/FormInput/FormInput";
import { useMutation, useQueryClient } from "react-query";
import { passwordResetAPI } from "../../api/passwordReset";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { Alert } from "@mui/material";

interface PasswordResetInput {
  new_password1: string;
  new_password2: string;
  uid: string;
  token: string;
}

interface UrlParams {
  uid: string;
  key: string;
}

const schema = Yup.object({
  new_password1: Yup.string().min(8, "Password is too short - should be 8 chars minimum.").required("Password is required"),
  new_password2: Yup.string().oneOf([Yup.ref('new_password1'), null], 'Passwords not matching!').required("Password Confirmation Required!")
})

export default function PasswordResetForm() {
  let history = useHistory();

  let { uid, key } = useParams<UrlParams>();

  useEffect(() => {
    console.log(uid, key)
  }, [uid, key])

  const { control, handleSubmit, formState } = useForm<PasswordResetInput>({
    resolver: yupResolver(schema),
    mode: "onChange",
    defaultValues: {
      uid: uid,
      token: key,
    }
  })

  const [error, setError] = useState<any>(null);

  const query = useQueryClient();
  const userMutate = useMutation((data) => passwordResetAPI(data));

  useDocumentTitle("Password Reset");

  const onSubmit: SubmitHandler<PasswordResetInput> = async (
    data: PasswordResetInput
  ) => {
    // eslint-disable-next-line no-console

    // @ts-ignore
    userMutate.mutate(data, {
      onSuccess: () => {
        history.push("/accounts/login/");
        toast.success(`Password Changed Successfully!`);
      },
      onError: (err: any) => {
        toast.error(err.message);
        setError(err.message);
      },
    });
  };

  return (
    <ResponsiveForm
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      noValidate
    >
      {error !== null && (
        <Alert
          sx={{ marginBottom: (theme) => theme.spacing(1) }}
          severity="error"
        >
          {error}
        </Alert>
      )}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormInput
            name="new_password1"
            label="Password"
            type="password"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="new_password2"
            label="Confirm Password"
            type="password"
            control={control}
            required
          />
        </Grid>
        {/* <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
        </Grid> */}
        <Grid item xs={12}>
          <LoadingButton
            type="submit"
            loading={userMutate.isLoading}
            loadingPosition="end"
            fullWidth
            variant="contained"
            color="success"
            endIcon={<LockOpenIcon color="warning" />}
          >
            Reset Password!
          </LoadingButton>
        </Grid>
      </Grid>
    </ResponsiveForm>
  );
}