import React from "react";
import Box from "@mui/material/Box";
import { styled } from "@mui/material/styles";

const ResponsiveForm: any = styled(Box)(({ theme }) => ({
  [theme.breakpoints.up("sm")]: {
    width: 300,
  },
  [theme.breakpoints.down("sm")]: {
    width: 250,
  },
  [theme.breakpoints.down("xs")]: {
    width: 50,
  },
}));

export default ResponsiveForm;
