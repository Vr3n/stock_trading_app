import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Grid from "@mui/material/Grid";
import { toast } from "react-toastify";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useDocumentTitle } from "../../hooks/useDocumentTitle";
import ResponsiveForm from "../ResponsiveForm";
import FormInput from "../../Components/FormInput/FormInput";
import { useMutation, useQueryClient } from "react-query";
import { loginAPI } from "../../api/login";
import LoadingButton from "@mui/lab/LoadingButton";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { Alert } from "@mui/material";

interface LoginFormInput {
  email: string;
  password: string;
}

const schema = Yup.object({
  email: Yup.string()
    .email("Enter valid email format")
    .required("Email is required!"),
  password: Yup.string().required("Password is required"),
}).required();

export default function SignIn() {
  let history = useHistory();

  const { control, handleSubmit, formState } = useForm<LoginFormInput>({
    resolver: yupResolver(schema),
    mode: "onChange",
  });

  const [error, setError] = useState<any>(null);

  const query = useQueryClient();
  const userMutate = useMutation((data) => loginAPI(data), {
    onSuccess: () => query.invalidateQueries("user"),
  });

  useDocumentTitle("LogIn");

  const onSubmit: SubmitHandler<LoginFormInput> = async (
    data: LoginFormInput
  ) => {
    // eslint-disable-next-line no-console

    // @ts-ignore
    userMutate.mutate(data, {
      onSuccess: () => {
        history.push("/trading/");
        toast.success(`You have logged in successfully`);
      },
      onError: (err: any) => {
        toast.error(err.message);
        setError(err.message);
      },
    });
  };

  return (
    <ResponsiveForm
      component="form"
      onSubmit={handleSubmit(onSubmit)}
      noValidate
    >
      {error !== null && (
        <Alert
          sx={{ marginBottom: (theme) => theme.spacing(1) }}
          severity="error"
        >
          {error}
        </Alert>
      )}
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <FormInput
            name="email"
            label="Email address"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="password"
            label="Password"
            type="password"
            control={control}
            required
          />
        </Grid>
        {/* <Grid item xs={12}>
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
        </Grid> */}
        <Grid item xs={12}>
          <LoadingButton
            type="submit"
            loading={userMutate.isLoading}
            loadingPosition="end"
            fullWidth
            variant="contained"
            color="success"
            endIcon={<LockOpenIcon color="warning" />}
          >
            Log In
          </LoadingButton>
        </Grid>
      </Grid>
      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        spacing={1}
        sx={{
          mt: 2,
        }}
      >
        <Grid item xs>
          <Link to="/accounts/forgot-password/">Forgot password?</Link>
        </Grid>
        <Grid item>
          <Link to="/accounts/register/">
            {"Don't have an account? Sign Up"}
          </Link>
        </Grid>
      </Grid>
    </ResponsiveForm>
  );
}
