import React from "react";
import { Link, useHistory } from "react-router-dom";
import Grid from "@mui/material/Grid";
import ResponsiveForm from "../ResponsiveForm";
import { useDocumentTitle } from "../../hooks/useDocumentTitle";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import FormInput from "../../Components/FormInput/FormInput";
import * as Yup from "yup";
import { toast } from "react-toastify";
import LoadingButton from "@mui/lab/LoadingButton";
import { registerUserAPI } from "../../api/registerUser";
import { useMutation, useQueryClient } from "react-query";
import HowToRegIcon from "@mui/icons-material/HowToReg";

const schema = Yup.object().shape({
  first_name: Yup.string()
    .required("First Name is required")
    .matches(/^([^0-9]*)$/, "Should not contain numbers.")
    .matches(
      /^([^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*)$/,
      "Should not contain special characters."
    ),
  last_name: Yup.string()
    .matches(/^([^0-9]*)$/, "Should not contain numbers.")
    .matches(
      /^([^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*)$/,
      "Should not contain special characters."
    ),
  mobile_number: Yup.string()
    .required("Mobile number is required")
    .matches(/^([^A-Za-z]*)$/, "Mobile number should contain only digits")
    .matches(
      /^([^!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*)$/,
      "Should not contain special characters."
    )
    .min(10, "Mobile number length should be 10")
    .max(10, "Mobile number length should not exceed more than 10."),
  email: Yup.string()
    .email("Enter valid email format")
    .required("Email is required!"),
  password1: Yup.string().required("Password is required"),
  password2: Yup.string()
    .required("Password is required")
    .oneOf([Yup.ref("password1"), null], "Password doesn't match."),
});

interface SignUpInputs {
  first_name: string;
  last_name: string;
  email: string;
  mobile_number: string;
  password1: string;
  password2: string;
}

export default function SignUp() {
  useDocumentTitle("SignUp");

  let history = useHistory();

  const { handleSubmit, control, formState } = useForm({
    resolver: yupResolver(schema),
    mode: "onChange",
  });

  const query = useQueryClient();
  const userRegisterMutate = useMutation((data) => registerUserAPI(data), {
    onSuccess: () => query.invalidateQueries("user"),
  });

  const onSubmit: SubmitHandler<SignUpInputs> = async (data: SignUpInputs) => {
    // @ts-ignore
    userRegisterMutate.mutate(data, {
      onSuccess: () => {
        history.push("/accounts/registration-confirmed/");
        toast.success("User registered successfully");
      },
      onError: (err: any) => {
        toast.error(err.message);
      },
    });
  };

  return (
    <ResponsiveForm
      component="form"
      noValidate
      onSubmit={handleSubmit(onSubmit)}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <FormInput
            name="first_name"
            autoFocus
            label="First Name"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <FormInput name="last_name" label="Last Name" control={control} />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="email"
            label="Email Address"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="mobile_number"
            label="Mobile Number"
            control={control}
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="password1"
            label="Password"
            control={control}
            type="password"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <FormInput
            name="password2"
            label="Confirm Password"
            control={control}
            type="password"
            required
          />
        </Grid>
        <Grid item xs={12}>
          <LoadingButton
            type="submit"
            loading={userRegisterMutate.isLoading}
            loadingPosition="end"
            fullWidth
            variant="contained"
            color="success"
            endIcon={<HowToRegIcon color="warning" />}
          >
            Register
          </LoadingButton>
        </Grid>
      </Grid>
      <Grid container justifyContent="center">
        <Grid item>
          <Link to="/accounts/login/">Already have an account? Sign in</Link>
        </Grid>
      </Grid>
    </ResponsiveForm>
  );
}
