import React from "react";
import Container from "@mui/material/Container";
import CssBaseline from "@mui/material/CssBaseline";
import Paper from "@mui/material/Paper";
import Avatar from "@mui/material/Avatar";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import CheckCircleOutlineIcon from "@mui/icons-material/CheckCircleOutline";
import Copyright from "../utils/Copyright";
import { useDocumentTitle } from "../hooks/useDocumentTitle";
import OtpForm from "../Forms/OtpForm/OtpForm";

const EmailConfirmation = () => {
  useDocumentTitle("Registered Successfully!");

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Paper
        elevation={5}
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          p: 2,
          alignItems: "center",
          textAlign: "center",
        }}
      >
        <Avatar
          sx={{
            m: 1,
            mt: 3,
            width: 54,
            height: 54,
            backgroundColor: "secondary.main",
          }}
        >
          <CheckCircleOutlineIcon />
        </Avatar>
        <Typography component="h1" variant="h5" gutterBottom>
          Email Sent Successfully to the Registered mail.
        </Typography>
        <Divider />
        {/* <OtpForm /> */}
        {/* <Divider /> */}
        <Typography variant="body1">
          You'll be sent to sign in page after, verifying the email.
        </Typography>
        <Copyright
          sx={{
            mt: 8,
            mb: 3,
            color: (theme: any) => theme.palette.secondary.dark,
          }}
        />
      </Paper>
    </Container>
  );
};

export default EmailConfirmation;
