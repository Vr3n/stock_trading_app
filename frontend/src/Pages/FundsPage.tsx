import React, { useEffect } from "react";

import { useDocumentTitle } from "../hooks/useDocumentTitle";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { useUserFundQuery } from "../hooks/useUserFundsQuery";
import LinearProgress from "@mui/material/LinearProgress";
import { floatingPoint } from "../utils/floatingPoint";

const SecondPage = () => {
  // Defining document title.
  useDocumentTitle("Funds");

  // loading the user funds.
  const {
    data: userFundsData,
    isLoading: isUserFundsLoading,
    isError: isUserFundsError,
    error: userFundsError,
  } = useUserFundQuery((data: any) => data);

  useEffect(() => {
    if (userFundsData) {
      console.log(userFundsData[0]);
    }
  }, [userFundsData]);

  return isUserFundsLoading ? (
    <LinearProgress />
  ) : (
    <Stack spacing={1} divider={<Divider />}>
      <Typography variant="h4" component="h3">
        Funds 💰
      </Typography>
      <Paper
        elevation={2}
        sx={{
          p: 2,
        }}
      >
        <Stack spacing={2} divider={<Divider />}>
          <Stack spacing={2} divider={<Divider />}>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1" component="h3">
                Available margin
              </Typography>
              <Typography variant="h5" component="h3">
                {floatingPoint(userFundsData[0].available_margin)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1" component="h3">
                Used margin
              </Typography>
              <Typography variant="h5" component="h3">
                {floatingPoint(userFundsData[0].used_margin)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1" component="h3">
                Available cash
              </Typography>
              <Typography variant="h5" component="h3">
                {floatingPoint(userFundsData[0].available_cash)}
              </Typography>
            </Stack>
          </Stack>
          <Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Opening balance</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].opening_balance)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Payin</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].pay_in)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">PayOut</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].pay_out)}
              </Typography>
            </Stack>
            {/* <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Payout</Typography>
              <Typography variant="body1">0.00</Typography>
            </Stack> */}
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">SPAN</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].span)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Delivery margin</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].delivery_margin)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Exposure</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].exposure)}
              </Typography>
            </Stack>
            <Stack
              justifyContent="space-between"
              alignItems="flex-start"
              direction="row"
              sx={{ paddingBottom: (theme) => theme.spacing(1) }}
            >
              <Typography variant="body1">Options premium</Typography>
              <Typography variant="body1">
                {floatingPoint(userFundsData[0].options_premium)}
              </Typography>
            </Stack>
          </Stack>
        </Stack>
      </Paper>
    </Stack>
  );
};

export default SecondPage;
