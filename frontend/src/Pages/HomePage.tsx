import React, { useEffect, useRef } from "react";
import Emoji from "a11y-react-emoji";
import { useDocumentTitle } from "../hooks/useDocumentTitle";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardHeader from "@mui/material/CardHeader";
import chartData from "../sections/Chart/series";
import CandleStickChart from "../Components/CandleStickChart/CandleStickChart";
import OrderTable from "../Tables/OrderTable";

const HomePage = () => {
  useDocumentTitle("Home");

  return (
    <Box
      sx={{
        width: "100%",
      }}
    >
      <Stack spacing={2} divider={<Divider flexItem />}>
        {/* <Typography variant="h6" component="h1">
          Hello, Viren Patel! <Emoji symbol="👋" />
        </Typography> */}
        <Paper
          elevation={2}
          sx={{
            p: 3,
          }}
        >
          <CandleStickChart
            options={chartData.options}
            series={chartData.series}
            height={350}
          />
        </Paper>
        <Paper elevation={2}>
          <OrderTable defaultPageSize={5} paginate={false} />
        </Paper>
      </Stack>
    </Box>
  );
};

export default HomePage;
