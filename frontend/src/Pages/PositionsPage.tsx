import React from "react";

import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import { faInbox } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDocumentTitle } from "../hooks/useDocumentTitle";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import PositionsTable from "../Tables/PositionsTable";
// import OrderTable from "../Tables/OrderTable";

const PositionsPage = () => {
  // Defining document title.
  useDocumentTitle("Positions");

  return (
    <Stack spacing={1} divider={<Divider />}>
      <Typography variant="h4" component="h1">
        Your Positions
      </Typography>
      <Paper
        elevation={2}
        sx={{
          p: 2,
        }}
      >
        <PositionsTable defaultPageSize={15} />
      </Paper>
    </Stack>
  );
};

export default PositionsPage;
