import React, { useEffect } from "react";
import Paper from "@mui/material/Paper";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import LinearProgress from "@mui/material/LinearProgress";
import Divider from "@mui/material/Divider";
import LetterAvatar from "../Components/LetterAvatar/LetterAvatar";
import { useUserInfoQuery } from "../hooks/useUserInfo";
import { useDocumentTitle } from "../hooks/useDocumentTitle";
import KycDetail from "../Components/KycDetail/KycDetail";
import UserBanks from "../Components/UserBanks/UserBanks";

const ProfilePage = () => {
  const {
    data: userInfoData,
    isLoading: isUserLoading,
    isError: isUserError,
    error: userError,
  } = useUserInfoQuery();

  useDocumentTitle(`User Profile`);

  return isUserLoading ? (
    <LinearProgress />
  ) : (
    <>
      <Stack spacing={2} divider={<Divider />}>
        <Paper
          sx={{
            p: 5,
            alignItems: "center",
          }}
        >
          <Stack spacing={2} divider={<Divider />}>
            <Stack
              spacing={1}
              direction="row"
              divider={<Divider orientation="vertical" flexItem />}
            >
              <LetterAvatar
                name={`${userInfoData.first_name} ${userInfoData.last_name}`}
                size="256"
              />
              <Typography variant="h3">
                {userInfoData.first_name} {userInfoData.last_name}
              </Typography>
            </Stack>
            <Paper
              sx={{
                p: 5,
                alignItems: "center",
              }}
              elevation={2}
            >
              <Typography gutterBottom variant="h5">
                Personal Details
              </Typography>
              <Divider />
              <Stack direction="row" sx={{ mt: (theme) => theme.spacing(2) }}>
                <Typography variant="body1">
                  First Name: &nbsp; <b>{userInfoData.first_name}</b>
                </Typography>
              </Stack>
              <Stack direction="row" sx={{ mt: (theme) => theme.spacing(2) }}>
                <Typography variant="body1">
                  Last Name: &nbsp; <b>{userInfoData.last_name}</b>
                </Typography>
              </Stack>
              <Stack direction="row" sx={{ mt: (theme) => theme.spacing(2) }}>
                <Typography variant="body1">
                  Email: &nbsp; <b>{userInfoData.email}</b>
                </Typography>
              </Stack>
              <Stack direction="row" sx={{ mt: (theme) => theme.spacing(2) }}>
                <Typography variant="body1">
                  Mobile Number: &nbsp; <b>{userInfoData.mobile_number}</b>
                </Typography>
              </Stack>
            </Paper>
          </Stack>
        </Paper>
        <KycDetail />
        <UserBanks user_id={userInfoData.id} />
      </Stack>
    </>
  );
};

export default ProfilePage;
