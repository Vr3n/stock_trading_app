import React, { useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Copyright from "../utils/Copyright";
import SignIn from "../Forms/SignInForm/SignInForm";
import SignUp from "../Forms/SignUpForm/SignUpForm";
import ForgotPasswordForm from "../Forms/ForgotPasswordForm/ForgotPasswordForm";
import PasswordResetForm from "../Forms/PasswordResetForm/PasswordResetForm";
import LogoutForm from "../Forms/LogoutForm/LogoutForm";
import { useUserInfoQuery } from "../hooks/useUserInfo";
import { useHistory } from "react-router-dom";
import CircularProgress from "@mui/material/CircularProgress";
import GCSLogo from "Imgs/gcs_logo@0.5x.png";
import { styled } from "@mui/material/styles";

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

type RegistrationPageProp = {
  form: "signin" | "signup" | "forgot_password" | "logout" | "reset_password";
};

const RegistrationPages: React.FC<RegistrationPageProp> = ({ form }) => {
  const {
    data: userInfoData,
    isLoading: isUserInfoLoading,
    isFetching: isUserInfoFetching,
  } = useUserInfoQuery();

  let history = useHistory();

  useEffect(() => {
    if (userInfoData !== undefined) {
      {
        userInfoData.isAuthenticated && history.push("/trading/");
      }
    }
  }, [userInfoData, isUserInfoLoading]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      {isUserInfoLoading ? (
        <CircularProgress />
      ) : (
        <Paper
          elevation={5}
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar
            sx={{
              m: 1,
              mt: 3,
              backgroundColor: "#ebffe6",
              width: 54,
              height: 54,
              p: 1,
            }}
          >
            <Img src={GCSLogo} alt="Greencurve Logo" />
          </Avatar>
          <Typography
            component="h1"
            variant="h5"
            sx={{
              mb: 3,
            }}
          >
            {form === "signin" && "Sign in"}
            {form === "signup" && "Sign Up"}
            {form === "forgot_password" && "Forgot Password"}
            {form === "logout" && "Log Out"}
          </Typography>
          {form === "signin" && <SignIn />}
          {form === "signup" && <SignUp />}
          {form === "forgot_password" && <ForgotPasswordForm />}
          {form === "logout" && <LogoutForm />}
          {form === "reset_password" && <PasswordResetForm />}
          <Copyright
            sx={{
              mt: 8,
              mb: 3,
              color: (theme: any) => theme.palette.secondary.dark,
            }}
          />
        </Paper>
      )}
    </Container>
  );
};

export default RegistrationPages;
