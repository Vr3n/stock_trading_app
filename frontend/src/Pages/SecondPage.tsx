import React from "react";

import { useDocumentTitle } from "../hooks/useDocumentTitle";
import Divider from "@mui/material/Divider";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import OrderTable from "../Tables/OrderTable";

const SecondPage = () => {
  // Defining document title.
  useDocumentTitle("Orders");

  return (
    <Stack spacing={1} divider={<Divider />}>
      <Typography variant="h4" component="h1">
        Your orders
      </Typography>
      <Paper
        elevation={2}
        sx={{
          p: 2,
        }}
      >
        <OrderTable defaultPageSize={15} />
      </Paper>
    </Stack>
  );
};

export default SecondPage;
