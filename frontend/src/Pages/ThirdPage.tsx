import React from "react";
import Typography from "@mui/material/Typography";
import Emoji from "a11y-react-emoji";
import { useDocumentTitle } from "../hooks/useDocumentTitle";

const ThirdPage = () => {
  useDocumentTitle("Third Page 🤖");
  return (
    <Typography variant="h3" component="h1">
      Third Page <Emoji symbol="🤖" />
    </Typography>
  );
};

export default ThirdPage;
