import React, { useEffect, useMemo, useState } from "react";
import { useOrderQuery } from "../hooks/useOrderQuery";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Chip from "@mui/material/Chip";

import { toast } from "react-toastify";
import DataTable from "../Components/DataTable/DataTable";
import EditIcon from "@mui/icons-material/Edit";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import CurrencyRupeeIcon from "@mui/icons-material/CurrencyRupee";
import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import { useMutation, useQueryClient } from "react-query";
import { faInbox } from "@fortawesome/free-solid-svg-icons";
import ModifyOrderDialog from "../Components/OrderDialog/ModifyOrderDialog";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { orderDelete } from "../api/orderDelete";
import DeleteOrderModal from "../Components/DeleteModal/DeleteOrderModal";

type ModifyButtonProps = {
  onClick: any;
  status: any;
};

const ModifyButton: React.FC<ModifyButtonProps> = ({ onClick, status }) => {
  return (
    <Button
      disabled={status === "Executed"}
      endIcon={<EditIcon />}
      onClick={onClick}
      color="primary"
      variant="contained"
    >
      Modify
    </Button>
  );
};

const DeleteButton: React.FC<ModifyButtonProps> = ({ onClick, status }) => {
  return (
    <Button
      endIcon={<DeleteForeverIcon />}
      disabled={status === "Executed"}
      onClick={onClick}
      color="error"
      variant="outlined"
    >
      Delete
    </Button>
  );
};

type OrderActionChipProps = {
  orderAction: "BUY" | "SELL";
};

const OrderActionChips: React.FC<OrderActionChipProps> = ({ orderAction }) => {
  return (
    <Chip
      icon={
        orderAction === "BUY" ? <ShoppingCartIcon /> : <CurrencyRupeeIcon />
      }
      label={orderAction}
      color={orderAction === "BUY" ? "primary" : "warning"}
    />
  );
};

type OrderTablePropType = {
  defaultPageSize: number;
  paginate?: boolean;
};

type OrderData = {
  id: string;
  code: string;
  price: string;
  stop_loss: string;
  stockId: any;
  orderAction: "buy" | "sell";
  quantity: any;
  margin: any;
  product: any;
  leverage: any;
  order_type: any;
};

const OrderTable: React.FC<OrderTablePropType> = ({
  defaultPageSize,
  paginate = true,
}) => {
  const {
    data: orderData,
    isLoading: isOrderLoading,
    isError: isOrderError,
    error: orderError,
  } = useOrderQuery((data: any) => data);

  const [open, setOpen] = useState<boolean>(false);
  const [deleteOpen, setDeleteOpen] = useState<boolean>(false);
  const [orderRowData, setOrderRowData] = useState<OrderData>({
    id: "",
    code: "",
    price: "",
    stop_loss: "",
    stockId: "",
    quantity: "",
    orderAction: "buy",
    margin: "",
    leverage: "",
    product: "",
    order_type: "",
  });

  const handleModifyOpen = ({
    id,
    code,
    price,
    stockId,
    stop_loss,
    orderAction,
    quantity,
    margin,
    product,
    leverage,
    order_type,
  }: OrderData) => {
    setOrderRowData({
      id,
      code,
      price,
      stop_loss,
      stockId,
      orderAction,
      quantity,
      margin,
      product,
      leverage,
      order_type,
    });
    setOpen(true);
  };

  const handleDeleteOpen = ({
    id,
    code,
    price,
    stockId,
    stop_loss,
    orderAction,
    quantity,
    margin,
    product,
    leverage,
    order_type,
  }: OrderData) => {
    setOrderRowData({
      id,
      code,
      price,
      stop_loss,
      stockId,
      orderAction,
      quantity,
      margin,
      product,
      leverage,
      order_type,
    });
    setDeleteOpen(true);
  };

  const handleModifyClose = () => setOpen(false);
  const handleDeleteClose = () => setDeleteOpen(false);

  const columns = useMemo(
    () => [
      {
        accessor: "id",
        Header: "Order Id",
      },
      {
        accessor: "stock_code",
        Header: "Stock",
      },
      {
        accessor: "quantity",
        Header: "Quantity",
      },
      {
        accessor: "price",
        Header: "Price",
      },
      {
        accessor: "stop_loss",
        Header: "Stop Loss",
      },
      {
        accessor: "margin",
        Header: "Margin",
      },
      {
        accessor: "action",
        Header: "Action",
        Cell: (props: any) => (
          <OrderActionChips orderAction={props.row.values.action} />
        ),
      },
      {
        accessor: "order_type",
        Header: "Order Type",
      },
      {
        accessor: "leverage",
        Header: "Leverage",
      },
      {
        accessor: "product",
        Header: "Product",
      },
      {
        accessor: "status",
        Header: "Status",
      },
      {
        Header: "Modify",
        accessor: "modify",
        Cell: (props: any) => {
          return (
            <ModifyButton
              status={props.row.values.status}
              onClick={() => {
                handleModifyOpen({
                  id: props.row.values.id,
                  orderAction: props.row.values.action.toLowerCase(),
                  stop_loss: props.row.values.stop_loss,
                  stockId: props.row.values.stock,
                  code: props.row.values.stock_code,
                  price: props.row.values.price,
                  quantity: props.row.values.quantity,
                  margin: props.row.values.margin,
                  leverage: props.row.values.leverage,
                  product: props.row.values.product,
                  order_type: props.row.values.order_type,
                });
              }}
            />
          );
        },
      },
      {
        Header: "Delete",
        accessor: "delete",
        Cell: (props: any) => {
          return (
            <DeleteButton
              status={props.row.values.status}
              onClick={() => {
                handleDeleteOpen({
                  id: props.row.values.id,
                  orderAction: props.row.values.action.toLowerCase(),
                  stop_loss: props.row.values.stop_loss,
                  stockId: props.row.values.stock,
                  code: props.row.values.stock,
                  price: props.row.values.price,
                  quantity: props.row.values.quantity,
                  margin: props.row.values.margin,
                  product: props.row.values.product,
                  leverage: props.row.values.leverage,
                  order_type: props.row.values.order_type,
                });
              }}
            />
          );
        },
      },
    ],
    []
  );

  const data = useMemo(() => {
    return orderData;
  }, [orderData]);

  const query = useQueryClient();

  const deleteOrderMutation = useMutation((id) => orderDelete(id), {
    onSuccess: () => query.invalidateQueries("orders"),
  });

  useEffect(() => {
    if (isOrderError) {
      // @ts-ignore
      toast.error(error.message);
    }
  }, [isOrderError]);

  return (
    <>
      <DeleteOrderModal
        stop_loss={orderRowData.stop_loss}
        quantity={orderRowData.quantity}
        onClose={handleDeleteClose}
        orderId={orderRowData.id}
        orderState={orderRowData.orderAction}
        open={deleteOpen}
        code={orderRowData.code}
        price={orderRowData.price}
        stockId={orderRowData.stockId}
        deleteFunc={deleteOrderMutation}
      />
      <ModifyOrderDialog
        stop_loss={orderRowData.stop_loss}
        quantity={orderRowData.quantity}
        handleClose={handleModifyClose}
        orderId={orderRowData.id}
        orderState={orderRowData.orderAction}
        open={open}
        code={orderRowData.code}
        price={orderRowData.price}
        stockId={orderRowData.stockId}
        product={orderRowData.product}
        leverage={orderRowData.leverage}
        margin={orderRowData.margin}
        order_type={orderRowData.order_type}
      />
      {isOrderLoading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            padding: "0.5rem",
          }}
        >
          <CircularProgress />
        </div>
      ) : (
        <>
          {data !== undefined && data.length === 0 && (
            <Box
              sx={{
                textAlign: "center",
                cursor: "pointer",
                p: 2,
              }}
            >
              <IconButton size="large">
                <FontAwesomeIcon size="lg" color="#2255c2" icon={faInbox} />
              </IconButton>
              <Typography variant="h6" gutterBottom>
                You haven't placed any orders.
              </Typography>
            </Box>
          )}
          {data !== undefined && data.length > 0 && (
            <DataTable
              title={"Orders"}
              defaultPageSize={defaultPageSize}
              columns={columns}
              data={data}
              paginate={paginate}
              hiddenColumns={["id"]}
            />
          )}
        </>
      )}
    </>
  );
};

export default OrderTable;
