import React, { useEffect, useMemo, useState } from "react";
import { usePositionQuery } from "../hooks/usePositionsQuery";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Chip from "@mui/material/Chip";
import OptionsButton from "../Components/OptionsButton/OptionsButton";

import { toast } from "react-toastify";
import DataTable from "../Components/DataTable/DataTable";
import { faInbox } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// type ModifyButtonProps = {
//   onClick: any;
// };

// const ModifyButton: React.FC<ModifyButtonProps> = ({ onClick }) => {
//   return (
//     <Button
//       endIcon={<EditIcon />}
//       onClick={onClick}
//       color="primary"
//       variant="contained"
//     >
//       Modify
//     </Button>
//   );
// };

// const DeleteButton: React.FC<ModifyButtonProps> = ({ onClick }) => {
//   return (
//     <Button
//       endIcon={<DeleteForeverIcon />}
//       onClick={onClick}
//       color="error"
//       variant="outlined"
//     >
//       Delete
//     </Button>
//   );
// };

type ProductProps = {
  product: "Intraday" | "Carry Forward";
};

const ProductChips: React.FC<ProductProps> = ({ product }) => {
  return (
    <Chip
      label={product}
      variant="outlined"
      color={product === "Intraday" ? "primary" : "warning"}
    />
  );
};

type PositionsPropType = {
  defaultPageSize: number;
};

// type OrderData = {
//   id: string;
//   code: string;
//   price: string;
//   stop_loss: string;
//   stockId: any;
//   orderAction: "buy" | "sell";
//   quantity: any;
//   margin: any;
//   product: any;
//   leverage: any;
// };

const PositionsTable: React.FC<PositionsPropType> = ({ defaultPageSize }) => {
  const {
    data: positionData,
    isLoading: ispositionLoading,
    isError: ispositionError,
    error: positionError,
  } = usePositionQuery((data: any) => data);

  //   const [open, setOpen] = useState<boolean>(false);
  //   const [deleteOpen, setDeleteOpen] = useState<boolean>(false);
  //   const [orderRowData, setOrderRowData] = useState<OrderData>({
  //     id: "",
  //     code: "",
  //     price: "",
  //     stop_loss: "",
  //     stockId: "",
  //     quantity: "",
  //     orderAction: "buy",
  //     margin: "",
  //     leverage: "",
  //     product: "",
  //   });

  //   const handleModifyOpen = ({
  //     id,
  //     code,
  //     price,
  //     stockId,
  //     stop_loss,
  //     orderAction,
  //     quantity,
  //     margin,
  //     product,
  //     leverage,
  //   }: OrderData) => {
  //     setOrderRowData({
  //       id,
  //       code,
  //       price,
  //       stop_loss,
  //       stockId,
  //       orderAction,
  //       quantity,
  //       margin,
  //       product,
  //       leverage,
  //     });
  //     setOpen(true);
  //   };

  //   const handleDeleteOpen = ({
  //     id,
  //     code,
  //     price,
  //     stockId,
  //     stop_loss,
  //     orderAction,
  //     quantity,
  //     margin,
  //     product,
  //     leverage,
  //   }: OrderData) => {
  //     setOrderRowData({
  //       id,
  //       code,
  //       price,
  //       stop_loss,
  //       stockId,
  //       orderAction,
  //       quantity,
  //       margin,
  //       product,
  //       leverage,
  //     });
  //     setDeleteOpen(true);
  //   };

  //   const handleModifyClose = () => setOpen(false);
  //   const handleDeleteClose = () => setDeleteOpen(false);

  const columns = useMemo(
    () => [
      {
        accessor: "id",
        Header: "Position id",
      },
      {
        accessor: "order.product.product",
        Header: "Product",
        Cell: (props: any) => (
          props.row.values["order.product.product"] !== undefined && <ProductChips product={props.row.values["order.product.product"]} />
        ),
      },
      {
        accessor: "order.stock",
        Header: "Symbol",
      },
      {
        accessor: "pl",
        Header: "PL",
      },
      {
        accessor: "ltp",
        Header: "LTP",
      },
      {
        accessor: "options",
        Header: "Options",
        Cell: (props: any) => (
          <OptionsButton />
        )
      },
      //   {
      //     Header: "Modify",
      //     accessor: "modify",
      //     Cell: (props: any) => {
      //       return (
      //         <ModifyButton
      //           onClick={() => {
      //             handleModifyOpen({
      //               id: props.row.values.id,
      //               orderAction: props.row.values.action.toLowerCase(),
      //               stop_loss: props.row.values.stop_loss,
      //               stockId: props.row.values.stock,
      //               code: props.row.values.stock,
      //               price: props.row.values.price,
      //               quantity: props.row.values.quantity,
      //               margin: props.row.values.margin,
      //               leverage: props.row.values.leverage,
      //               product: props.row.values.product,
      //             });
      //           }}
      //         />
      //       );
      //     },
      //   },
      //   {
      //     Header: "Delete",
      //     accessor: "delete",
      //     // Cell: (props: any) => {
      //     //   return (
      //     //     <DeleteButton
      //     //       onClick={() => {
      //     //         handleDeleteOpen({
      //     //           id: props.row.values.id,
      //     //           orderAction: props.row.values.action.toLowerCase(),
      //     //           stop_loss: props.row.values.stop_loss,
      //     //           stockId: props.row.values.stock,
      //     //           code: props.row.values.stock,
      //     //           price: props.row.values.price,
      //     //           quantity: props.row.values.quantity,
      //     //           margin: props.row.values.margin,
      //     //           product: props.row.values.product,
      //     //           leverage: props.row.values.leverage,
      //     //         });
      //     //       }}
      //     //     />
      //     //   );
      //     // },
      //   },
    ],
    []
  );

  const data = useMemo(() => {
    return positionData;
  }, [positionData]);

  //   const query = useQueryClient();

  //   const deletepositionMutation = useMutation((id) => positionDelete(id), {
  //     onSuccess: () => query.invalidateQueries("positions"),
  //   });

  useEffect(() => {
    if (ispositionError) {
      // @ts-ignore
      toast.error(error.message);
    }
  }, [ispositionError]);

  return (
    <>
      {/* <DeleteOrderModal
        stop_loss={orderRowData.stop_loss}
        quantity={orderRowData.quantity}
        onClose={handleDeleteClose}
        orderId={orderRowData.id}
        orderState={orderRowData.orderAction}
        open={deleteOpen}
        code={orderRowData.code}
        price={orderRowData.price}
        stockId={orderRowData.stockId}
        deleteFunc={deleteOrderMutation}
      />
      <ModifyOrderDialog
        stop_loss={orderRowData.stop_loss}
        quantity={orderRowData.quantity}
        handleClose={handleModifyClose}
        orderId={orderRowData.id}
        orderState={orderRowData.orderAction}
        open={open}
        code={orderRowData.code}
        price={orderRowData.price}
        stockId={orderRowData.stockId}
        product={orderRowData.product}
        leverage={orderRowData.leverage}
        margin={orderRowData.margin}
      /> */}
      {ispositionLoading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            alignItems: "center",
            padding: "0.5rem",
          }}
        >
          <CircularProgress />
        </div>
      ) : (
        <>
          {data !== undefined && data.length === 0 && (
            <Box
              sx={{
                textAlign: "center",
                cursor: "pointer",
                p: 2,
              }}
            >
              <IconButton size="large">
                <FontAwesomeIcon size="lg" color="#2255c2" icon={faInbox} />
              </IconButton>
              <Typography variant="h6" gutterBottom>
                You haven't placed any Positions.
              </Typography>
            </Box>
          )}
          {data !== undefined && data.length > 0 && (
            <DataTable
              title={"Positions"}
              defaultPageSize={defaultPageSize}
              columns={columns}
              data={data}
              hiddenColumns={["id"]}
            />
          )}
        </>
      )}
      ;
    </>
  );
};

export default PositionsTable;
