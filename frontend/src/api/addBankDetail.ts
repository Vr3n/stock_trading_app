import axios from "../axios";

export const addBankDetail = async (data: any) => {
  const res = await axios.post("/api/user/bank/create/", data).catch((error) => {
    for (const prop in error.response.data) {
      const err = error.response.data[prop];
      throw new Error(err);
    }
  });

  // @ts-ignore
  return await res.data;
};
