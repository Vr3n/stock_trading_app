import axios from "../axios";

export const checkUserAuthenticated = async () => {
  const res = await axios.get("/api/user_authenticated/");
  return await res.data;
};
