import Nomics from "nomics";

const nomics = new Nomics({
  apiKey: "e72b5b93288fa2e7afed91c081aafaec85870e00",
});

export const fetchCryptoData = async () => {
  try {
    const currencies = await nomics.currenciesTicker();

    let data = [];

    for (let i = 0; i < 11; ++i) {
      data.push(currencies[i]);
    }

    return data;
  } catch (e) {}
};
