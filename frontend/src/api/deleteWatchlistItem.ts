import axios from "../axios";

export const deleteWatchlistItem = async (code: any) => {
  const res = await axios
    .delete(`/api/watchlist/item/delete/${code}`)
    .catch((error) => {
      for (const prop in error.response.data) {
        const err = error.response.data[prop];
        throw new Error(err);
      }
    });

  // @ts-ignore
  return await res.data;
};
