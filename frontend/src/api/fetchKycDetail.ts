import axios from "../axios";

export const fetchKycDetail = async (signal: any) => {
  const res = await axios.get("/api/kyc/", { signal }).catch((error) => {
    for (const prop in error.response.data) {
      const err = error.response.data[prop];
      throw new Error(err);
    }
  });

  // @ts-ignore
  return await res.data;
};
