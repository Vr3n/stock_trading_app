import axios from "../axios";

export const fetchKycDocumentList = async (signal: any) => {
  const res = await axios.get("/api/kyc_document_list/", { signal }).catch((error) => {
    for (const prop in error.response.data) {
      const err = error.response.data[prop];
      throw new Error(err);
    }
  });

  // @ts-ignore
  return await res.data;
};