import axios from "../axios";

export const fetchOrders = async (signal: any) => {
  const res = await axios.get("/api/orders/", { signal });
  return await res.data;
};
