import axios from "../axios";

export const fetchPositions = async (signal: any) => {
  const res = await axios.get("/api/positions/", { signal });
  return await res.data;
};
