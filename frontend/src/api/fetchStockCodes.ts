import axios from "../axios";

export const fetchStockCodes = async () => {
  const res = await axios.get("/api/stock/list/");
  return await res.data;
};