import axios from "../axios";

export const fetchUser = async (signal: any) => {
  const res = await axios.get("/api/user_authenticated/", { signal });
  return await res.data;
};
