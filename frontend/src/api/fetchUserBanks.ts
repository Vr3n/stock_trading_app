
import axios from "../axios";

export const fetchUserBanks = async (signal: any) => {
  const res = await axios.get("/api/user/banks/", { signal });
  return await res.data;
};