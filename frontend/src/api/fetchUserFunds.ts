import axios from '../axios';

export const fetchUserFunds = async (signal: any) => {
    const res = await axios.get("/api/funds/", { signal });
    return await res.data;
}