import axios from "../axios";

export const fetchWatchList = async (signal: any) => {
  const res = await axios.get("/api/watchlist/", { signal });
  return await res.data;
};
