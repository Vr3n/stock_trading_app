import axiosInstance from "../axios";

export const forgotPasswordAPI = async (data: any) => {
  const res = await axiosInstance
    .post("api/users/password/reset/", data)
    .catch((error) => {
      for (const prop in error.response.data) {
        const err = error.response.data[prop];
        throw new Error(err);
      }
    });

  // @ts-ignore
  return await res.data;
};
