import axiosInstance from "../axios";

export const logoutAPI = async () => {
  const res = await axiosInstance.post("/api/users/logout/").catch((error) => {
    for (const prop in error.response.data) {
      const err = error.response.data[prop];
      throw new Error(err);
    }
  });

  // @ts-ignore
  return await res.data;
};
