import axios from "../axios";

export const orderDelete = async (id: any) => {
  const res = await axios.delete(`/api/orders/delete/${id}`).catch((error) => {
    for (const prop in error.response.data) {
      const err = error.response.data[prop];
      throw new Error(err);
    }
  });

  // @ts-ignore
  return await res.data;
};
