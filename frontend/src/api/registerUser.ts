import axiosInstance from "../axios";

export const registerUserAPI = async (data: any) => {
  const res = await axiosInstance
    .post("/api/users/accounts/register/", data)
    .catch((error) => {
      for (const prop in error.response.data) {
        const err = error.response.data[prop];
        throw new Error(err);
      }
    });

  // @ts-ignore
  return res.data;
};
