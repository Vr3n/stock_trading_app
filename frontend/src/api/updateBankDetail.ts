import axios from "../axios";

export const updateBankDetail = async (data: any) => {
  const res = await axios
    .put(`/api/user/bank/${data.id}/`, data)
    .catch((error) => {
      for (const prop in error.response.data) {
        const err = error.response.data[prop];
        throw new Error(err);
      }
    });

  // @ts-ignore
  return await res.data;
};
