import axios from "../axios";

export const updateWatchList = async (data: any) => {
  const res = await axios.post("/api/watchlist/create/", data);
  return await res.data;
};
