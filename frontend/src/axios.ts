import axios from "axios";

let BASE_URL = "https://www.greencurvesecurities.com";

console.log(process.env.NODE_ENV)
if (process.env.NODE_ENV === "development") {
  BASE_URL = "http://127.0.0.1:8000";
}



const instance = axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-type": "application/json",
  },
});

instance.defaults.xsrfCookieName = "csrftoken";
instance.defaults.xsrfHeaderName = "X-CSRFTOKEN";
instance.defaults.withCredentials = true;

export default instance;
