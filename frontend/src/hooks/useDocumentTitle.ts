import { useRef, useEffect } from "react";

/**
 * Custom hook for Dynamic title change.
 *
 * @param {string} title - Title of the page.
 * @param {boolean} [prevailOnUnmount=false] - If you want to keep same title for all pages, pass this parameter as true.
 *
 * @example
 *    const Home = () => {
 *      useDocumentTitle('This is home 👽');
 *      return <h1>Hello</h1>
 *    }
 *
 */

export const useDocumentTitle = (title: string, prevailOnUnmount = false) => {
  const defaultTitle = useRef(document.title);

  useEffect(() => {
    document.title = title + " - Greencurve Securities";
  }, [title]);

  useEffect(
    () => () => {
      if (!prevailOnUnmount) {
        document.title = defaultTitle.current;
      }
    },
    []
  );
};
