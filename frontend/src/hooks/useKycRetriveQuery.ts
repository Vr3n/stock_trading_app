import { useQuery } from "react-query";
import { fetchKycDetail } from "../api/fetchKycDetail";

export const useKycRetriveQuery = (select: any) =>
  // @ts-ignore
  useQuery(["kyc"], ({ signal }) => fetchKycDetail(signal), { select: select });
