import { useQuery } from "react-query";
import { fetchOrders } from "../api/fetchOrders";

export const useOrderQuery = (select: any) =>
  // @ts-ignore
  useQuery(["orders"], ({ signal }) => fetchOrders(signal), { select: select });
