import { useQuery } from "react-query";
import { fetchPositions } from "../api/fetchPositions";

export const usePositionQuery = (select: any) =>
  // @ts-ignore
  useQuery(["positions"], ({ signal }) => fetchPositions(signal), { select: select });