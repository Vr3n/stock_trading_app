import { useQuery } from "react-query";
import { fetchUserBanks } from "../api/fetchUserBanks";

export const useUserBankQuery = (select: any) =>
  // @ts-ignore
  useQuery(["banks"], ({ signal }) => fetchUserBanks(signal), {
    select: select,
  });
