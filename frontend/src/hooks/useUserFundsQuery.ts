import { useQuery } from "react-query";
import { fetchUserFunds } from "../api/fetchUserFunds";

export const useUserFundQuery = (select: any) =>
  // @ts-ignore
  useQuery(["user_funds"], ({ signal }) => fetchUserFunds(signal), {
    select: select,
    refetchOnMount: "always",
  });
