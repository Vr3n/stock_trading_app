import { useQuery } from "react-query";
import { fetchUser } from "../api/fetchUser";

export const useUserInfoQuery = () =>
  // @ts-ignore
  useQuery(["user"], ({ signal }) => fetchUser(signal));
