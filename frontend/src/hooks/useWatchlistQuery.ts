import { useQuery } from "react-query";
import { fetchWatchList } from "../api/fetchWatchList";

export const useWatchlistQuery = (select: any) =>
  // @ts-ignore
  useQuery(["watchlist"], ({ signal }) => fetchWatchList(signal), {
    select: select,
  });
