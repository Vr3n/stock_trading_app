import { useQuery } from "react-query";
import { fetchKycDocumentList } from "../api/fetchKycDocumentList";

export const useKycDocumentList = (select: any) =>
  // @ts-ignore
  useQuery(["kyc_documents"], ({ signal }) => fetchKycDocumentList(signal), { select: select });