import React from "react";
import { render } from "react-dom";
import { ThemeProvider } from "@mui/material/styles";
import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { theme } from "./theme";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

import App from "./App";
import RegistrationPages from "./Pages/RegistrationPages";
import EmailConfirmation from "./Pages/EmailConfirmation";

// Initializing Queryclient.
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

// rendering the application.
const appDiv = document.getElementById("app");

if (appDiv !== undefined) {
  render(
    <Router>
      <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <ToastContainer />
          <Switch>
            <Route path="/accounts/login/">
              <RegistrationPages form="signin" />
            </Route>
            <Route path="/accounts/register/">
              <RegistrationPages form="signup" />
            </Route>
            <Route path="/accounts/registration-confirmed/">
              <EmailConfirmation />
            </Route>
            <Route path="/accounts/forgot-password/">
              <RegistrationPages form="forgot_password" />
            </Route>
            <Route path="/accounts/reset/key/:uid-:key/">
              <RegistrationPages form="reset_password" />
            </Route>
            <Route path="/accounts/logout/">
              <RegistrationPages form="logout" />
            </Route>
            <Route path="/trading">
              <App />
            </Route>
          </Switch>
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </ThemeProvider>
    </Router>,
    appDiv
  );
}
