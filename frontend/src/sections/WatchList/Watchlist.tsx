import React, { useEffect, useState } from "react";
import Stack from "@mui/material/Stack";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import ButtonBase from "@mui/material/ButtonBase";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Modal from "@mui/material/Modal";
import SvgIcon from "@mui/material/SvgIcon";
import Typography from "@mui/material/Typography";

import WatchlistCard from "../../Components/WatchlistCard/WatchlistCard";
import StockSearchInput from "../../Components/SearchInput/StockSearchInput";
import OrderDialog from "../../Components/OrderDialog/OrderDialog";
import LinearProgress from "@mui/material/LinearProgress";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "react-toastify";
import { useWatchlistQuery } from "../../hooks/useWatchlistQuery";
import { TransitionGroup } from "react-transition-group";
import { useMutation, useQueryClient } from "react-query";
import DeleteModal from "../../Components/DeleteModal/DeleteModal";
import { deleteWatchlistItem } from "../../api/deleteWatchlistItem";
import { faListAlt } from "@fortawesome/free-solid-svg-icons";

type OrderData = {
  code: string;
  price: string;
  stockId: any;
  orderAction: "buy" | "sell";
};

type OrderDeleteData = {
  stockId: string;
  code: string;
};

const Watchlist = () => {
  const {
    data: watchlistData,
    isLoading,
    isError,
    error,
  } = useWatchlistQuery((data: any) => {
    if (data !== undefined && data.length > 0) {
      return data[0].stocks;
    }
    return data;
  });

  const [empty, setEmpty] = useState<boolean>(true);
  const [open, setOpen] = useState<boolean>(false);
  const [deleteOpen, setDeleteOpen] = useState<boolean>(false);
  const [deleteData, setDeleteData] = useState<OrderDeleteData>({
    stockId: "",
    code: "",
  });
  const [orderData, setOrderData] = useState<OrderData>({
    code: "",
    price: "",
    stockId: "",
    orderAction: "sell",
  });

  const handleDialogOpen = (orderData: OrderData) => {
    setOpen(true);
    setOrderData(orderData);
  };

  const handleWatchlistItemDelete = (orderData: OrderDeleteData) => {
    setDeleteOpen(true);
    setDeleteData(orderData);
  };

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };

  const query = useQueryClient();

  const deleteWatchlistItemMutation = useMutation(
    (code) => deleteWatchlistItem(code),
    {
      onSuccess: () => {
        return query.invalidateQueries("watchlist");
      },
    }
  );

  useEffect(() => {
    if (isError && error) {
      // @ts-ignore
      toast.error(error.message);
    }
  }, [isError, error]);

  const handleDialogClose = () => setOpen(false);

  return (
    <>
      <DeleteModal
        open={deleteOpen}
        code={deleteData.code}
        onClose={handleDeleteClose}
        deleteFunc={deleteWatchlistItemMutation}
      />
      <OrderDialog
        open={open}
        handleClose={handleDialogClose}
        orderState={orderData.orderAction}
        code={orderData.code}
        price={orderData.price}
        stockId={orderData.stockId}
      />
      <Stack
        sx={{
          marginTop: "0.938em",
        }}
        spacing={2}
      >
        <StockSearchInput />
        <Divider />
        <Stack spacing={1}>
          {isLoading && <LinearProgress color="primary" />}
          {error && (
            <Alert severity="error">
              <AlertTitle>Error</AlertTitle>
              {/* @ts-ignore */}
              {error.message}
            </Alert>
          )}
          <TransitionGroup>
            {/* //   ))
            // ) : (
            //   <Box
            //     sx={{
            //       textAlign: "center",
            //       cursor: "pointer",
            //     }}
            //     onClick={() => console.log("clicked")}
            //   >
            //     <IconButton size="large">
            //       <FontAwesomeIcon size="lg" color="#2255c2" icon={faListAlt} />
            //     </IconButton>
            //     <Typography variant="h6" gutterBottom>
            //       Watchlist is empty!
            //     </Typography>
            //     <Typography variant="subtitle1">
            //       Search for your favorite stocks from searchbar,
            //     </Typography>
            //     <Typography variant="subtitle2">
            //       and add them to watchlist!
            //     </Typography>
            //   </Box>
            // )} */}
            {watchlistData !== undefined &&
              watchlistData.map((stock: any) => (
                <Collapse key={stock.id}>
                  <Box
                    component="div"
                    sx={{
                      mb: (theme) => theme.spacing(1),
                    }}
                  >
                    <WatchlistCard
                      key={stock.id}
                      onClick={handleDialogOpen}
                      code={stock.code}
                      stockId={stock.name}
                      onDelete={handleWatchlistItemDelete}
                      percent1="10.36%"
                      price="1000.40"
                    />
                  </Box>
                </Collapse>
              ))}
          </TransitionGroup>
        </Stack>
      </Stack>
    </>
  );
};

export default Watchlist;
