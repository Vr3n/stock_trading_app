import { blue, green } from "@mui/material/colors";
import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  palette: {
    primary: {
      main: blue[600],
    },
    secondary: {
      main: green[300],
    },
  },
  typography: {
    fontFamily: "Open Sans",
    fontSize: 12,
  },
});
