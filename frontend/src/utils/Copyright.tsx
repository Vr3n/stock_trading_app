import React from "react";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";

export default function Copyright(props: any) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © "}
      <Link
        component="a"
        target="_blank"
        color="inherit"
        href="http://www.greencurvesecurities.com/"
      >
        Greencurve Securities
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}
