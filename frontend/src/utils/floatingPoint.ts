export const floatingPoint = (data: string) => parseFloat(data).toFixed(2);
