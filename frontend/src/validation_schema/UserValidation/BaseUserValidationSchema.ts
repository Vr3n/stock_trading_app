import * as Yup from "yup";


export const BaseUserValidationSchema = Yup.object({
  email: Yup.string().trim().email().required(),
  password: Yup.string().required("Password is required"),
})