from django.urls import path, re_path, reverse_lazy
from .views import (index, second_page, third_page, login,
                    register, forgot_password, registration_confirmed,
                    profile_page, home_page, positions_page, funds_page,
                    UserKycDocumentUploadView)
from allauth.account.views import PasswordResetFromKeyView

urlpatterns = [
    path('trading/', index, name="home"),
    path('trading/orders/', second_page, name="second_page"),
    path('trading/third/', third_page, name="third_page"),
    path("trading/profile/", profile_page, name="profile_page"),
    path("trading/positions/", positions_page, name="positions_page"),
    path("trading/funds/", funds_page, name="funds_page"),
    path("trading/kyc/update/<int:pk>/", UserKycDocumentUploadView.as_view(), name="user_kyc_upload"),
    re_path(r"^accounts/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$", PasswordResetFromKeyView.as_view(
        success_url=reverse_lazy('user_login')), name="password_reset_confirm"),
    path('accounts/login/', login, name="user_login"),
    path('accounts/register/', register, name="user_register"),
    path('accounts/forgot-password/', forgot_password, name="forgot-pasword"),
    path('accounts/registration-confirmed/',
         registration_confirmed, name="registration-confirmed"),
    path("", home_page, name="greencurve_home"),
]
