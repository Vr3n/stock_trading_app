from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import FormView
from api.models import UserDocument, KYCDocumentName, UserKYC
from backoffice.forms import UserDocumentForm

# Create your views here.


def home_page(request):
    return render(request, "index.html")


def login(request):
    return render(request, '_base.html')


def register(request):
    return render(request, '_base.html')


def registration_confirmed(request):
    return render(request, '_base.html')


def forgot_password(request):
    return render(request, '_base.html')


@login_required
def index(request):
    return render(request, '_base.html')


@login_required
def second_page(request):
    return render(request, "_base.html")


@login_required
def third_page(request):
    return render(request, "_base.html")


@login_required
def profile_page(request):
    return render(request, "_base.html")


@login_required
def positions_page(request):
    return render(request, "_base.html")


@login_required
def funds_page(request):
    return render(request, "_base.html")

class UserKycDocumentUploadView(LoginRequiredMixin, FormView):

    template_name = 'user_document_upload.html'
    form_class = UserDocumentForm
    model = UserDocument

    def get_success_url(self):
        return reverse_lazy('user_kyc_upload', kwargs={'pk': self.kwargs['pk']})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # check if pk is passed in kwargs.
        if 'pk' in self.kwargs:
            context['object'] = get_object_or_404(
                UserKYC, pk=self.kwargs['pk'])

        context["document_names"] = KYCDocumentName.objects.all()
        return context

    def form_valid(self, form):
        form.save()
        messages.success(self.request, "Document Uploaded Successfully",
                         extra_tags="alert alert-success alert-dismissible")
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, form.errors,
                       extra_tags="alert alert-error alert-dismissible")
        return super().form_invalid(form)