let fundSocket = null;


const fundConnect = function () {
  fundSocket = new WebSocket(
    "ws://" + window.location.host + "/ws/admin_fund_updates/"
  );

  fundSocket.onopen = function (e) {
    console.log("funds update connection successfully!");
  };

  fundSocket.onclose = function (e) {
    console.log("On close", e);
    console.log("Weboscket connection closed");
    setTimeout(() => {
      console.log("Reconnecting!");
      fundConnect();
    }, 2000);
  };

  fundSocket.onmessage = function (e) {
    console.log("On message", e);
    if (document.getElementById('all_users_funds') !== null) {
      htmx.trigger("#all_users_funds", "funds_update");
    }
    return false;
  };

  fundSocket.onerror = function (err) {
    console.log("WebSocket encountered an error: ");
    console.log(err);
    console.log("Closing the socket");
    fundSocket.close();
  };
};

fundConnect();
