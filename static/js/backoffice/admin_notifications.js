let chatSocket = null;
let audio_url = "http://127.0.0.1:8000/static/audio/apple.mp3";

const playSound = (url) => {
  const audio = new Audio(url);
  let promise = audio.play();
  if (promise !== undefined) {
    promise.then(_ => {
      audio.play();
    }).catch(error => {
      console.log(error);
    })
  }
};

const connect = function () {
  chatSocket = new WebSocket(
    "ws://" + window.location.host + "/ws/admin_notifications/"
  );

  chatSocket.onopen = function (e) {
    console.log("Notification connection established successfully!");
  };

  chatSocket.onclose = function (e) {
    console.log("Weboscket connection closed");
    setTimeout(() => {
      console.log("Reconnecting!");
      connect();
    }, 2000);
  };

  chatSocket.onmessage = function (e) {
    htmx.trigger("#notification_trigger", "notification_update");
    playSound(audio_url);
  };

  chatSocket.onerror = function (err) {
    console.log("WebSocket encountered an error: ");
    console.log(err);
    console.log("Closing the socket");
    chatSocket.close();
  };
};

connect();
