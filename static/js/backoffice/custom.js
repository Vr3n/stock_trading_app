$(document).ready(function () {
  $("#orderDataTable").DataTable();

  $("#unverified_mobile_table").DataTable();
  $("#unverified_email_table").DataTable();
  $("#unverified_kyc_table").DataTable();

  $("#all_users_table").DataTable();
  $("#all_emails_table").DataTable();
  $("#all_mobiles_table").DataTable();
  $("#all_kyc_table").DataTable();

  if ($("select#document_name")) {
    // Document select element
    let document_select = $("select#document_name");

    let aadhar_mask = new Inputmask("9999 9999 9999");
    let pancard_mask = new Inputmask({
      mask: "AAAAA 9999 A",
    });

    // Event listener for Element changes
    document_select.on("change", (e) => {
      // Check if The value isn't default one.
      if (e.target.value === "Select Document Type") {
        $("#document_form_toggle").css({
          display: "none",
        });
        return;
      }

      // Get the selected text and give output as per the selected textg.
      let selected_text = $(`option[value=${e.target.value}]`)[0].innerText;
      $("#document_form_toggle").css({
        display: "block",
      });

      // Select label for the number input.
      let input_label = $('label[for="id_number"]')[0];
      // prepending the selected text value to Input label.
      input_label.innerText = selected_text + " number";

      // Select the Document Input
      let input = $("input#id_number")[0];
      let jq_input = $("input#id_number");
      let jq_input_label = $('label[for="id_number"]');
      if (selected_text === "AADHAR CARD") {
        if (!jq_input.is(":visible")) {
          jq_input.show();
          jq_input_label.show();
          jq_input.addAttr("required");
        }
        if (!jq_input.prop("required")) {
          jq_input.prop("required", true);
        }
        aadhar_mask.mask(input);
      }

      if (selected_text === "Agreement") {
        jq_input.removeAttr("required");
        jq_input.hide();
        jq_input_label.hide();
      }

      if (selected_text === "Blank Cheque") {
        jq_input.removeAttr("required");
        jq_input.hide();
        jq_input_label.hide();
      }

      if (selected_text === "PAN CARD") {
        if (!jq_input.is(":visible")) {
          jq_input.show();
          jq_input_label.show();
        }
        if (!jq_input.prop("required")) {
          jq_input.prop("required", true);
        }
        pancard_mask.mask(input);
      }
    });
  }

  if ($("input#id_document")) {
    let document_input = $("input#id_document");

    document_input.on("change", () => {
      if (
        document_input[0].value.trim().toLowerCase() !== "" ||
        document_input[0].value !== null
      ) {
        const split_value = document_input[0].value.split(".");
        const val = split_value[split_value.length - 1];
        const VALID_FILETYPES = ["pdf", "png", "jpeg", "jpg"];
        if (!VALID_FILETYPES.includes(val.trim().toLowerCase())) {
          alert("this file type is not supported!");
          document_input.val("");
        }
      }
    });
  }

  // Decimal Input Masking.
  $("input.decimal").inputmask("decimal", {
    rightAlign: false,
    numericInput: true,
  });
});
