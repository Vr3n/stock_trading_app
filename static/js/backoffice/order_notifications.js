let orderSocket = null;


const orderConnect = function () {
  orderSocket = new WebSocket(
    "ws://" + window.location.host + "/ws/order_updates/"
  );

  orderSocket.onopen = function (e) {
    console.log("Order Notification connection established successfully!");
  };

  orderSocket.onclose = function (e) {
    console.log("Order Updates Connected!");
    setTimeout(() => {
      console.log("Reconnecting!");
      connect();
    }, 2000);
  };

  orderSocket.onmessage = function (e) {
    if (document.getElementById('orderDataTable') !== null) {
      htmx.trigger("#orderDataTable", "order_update");
    }
    if (document.getElementById('all_users_funds') !== null) {
      htmx.trigger("#all_users_funds", "funds_update");
    }
  };

  orderSocket.onerror = function (err) {
    console.log("WebSocket encountered an error: ");
    console.log(err);
    console.log("Closing the socket");
    orderSocket.close();
  };
};

orderConnect();
