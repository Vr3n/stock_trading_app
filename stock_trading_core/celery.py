import os
from celery import Celery
from celery.schedules import crontab
from django.apps import apps

os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                      "stock_trading_core.settings.dev")

# Get the base REDIS URL, default  to redis' default.
BASE_REDIS_URL = os.environ.get('REDIS_URL', 'redis://localhost:6379')

app = Celery('stock_trading_core')
app.conf.enable_utc = False
app.conf.update(timezone='Asia/Kolkata')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks(lambda: [n.name for n in apps.get_app_configs()])

app.conf.broker_url = BASE_REDIS_URL

# app.conf.beat_schedule = {
#     'fetch-every-2-seconds': {
#         'task': 'api.tasks.update_stock_prices',
#         'args': (['8KMILES', '3MINDIA', '20MICRONS'],),
#         'schedule': 2.0,
#         'options': {
#             'expired': 15.0,
#         }
#     },
# }
