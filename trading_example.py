import asyncio
import time
#import requests
from pix_apidata import *

api = apidata_lib.ApiData()
event_loop = asyncio.get_event_loop()

async def main():
    api.on_connection_started(connection_started)
    api.on_connection_stopped(connection_stopped)
    api.on_trade_update(on_trade)
    api.on_best_update(on_best)
    api.on_refs_update(on_refs)
    api.on_srefs_update(on_srefs)

    key = "/745y8A+XPshmfCGilWWDfgzrJo="
    host = "apidata.accelpix.in"
    scheme = "http"
    s = await api.initialize(key, host,scheme)
    print(s)

    his = await api.get_intra_eod("NIFTY-1","20210603", "20210604", "5")
    print("History : ",his)

    syms = ['NIFTY-1', 'BANKNIFTY-1', "INFY", "AARTIIND",]
    # symsGreeks = ["NIFTY2220318500CE"]
    # await api.subscribeGreeks(symsGreeks)
    await api.subscribeAll(syms)
    print("subscribe done")
    #needSnapshot = False
    #await api.subscribeSegments(needSnapshot)
    #time.sleep(1)
    #await api.unsubscribeAll(['NIFTY-1'])
    #print("Unsubscribe Done")

def on_trade(msg):
    trd = apidata_models.Trade(msg)
    print("Trade : ",msg) # or print(trd.volume) likewise object can be called for id, kind, ticker, segment, price, qty, oi

def on_best(msg):
    bst = apidata_models.Best(msg)
    print("Best : ",msg) # or print(bst.bidPrice) likewise object can be called for ticker, segmentId, kind, bidQty, askPrice, askQty

def on_refs(msg):
    ref = apidata_models.Refs(msg)
    print("Refs snapshot : ",msg) # or print(ref.price) likewise object can be called for segmentId, kind, ticker

def on_srefs(msg):
    sref = apidata_models.RefsSnapshot(msg)
    print("Refs update : ",msg) # or print(sref.high) likewise object can be called for kind, ticker, segmentId, open, close, low, avg, oi, lowerBand,upperBand

# def on_tradeSnapshot(msg):
#     trdSnap = apidata_models.Trade(msg)
#     print("TradeSnap : ",msg) # or print(trdSnap.volume) likewise object can be called for id, kind, ticker, segment, price, qty, oi

# def on_greeks(msg):
#     greeks = apidata_models.Greeks(msg)
#     print("OptionGreeks : ",msg) # or print(greeks.gamma) likewise object can be called for kind, ticker, iv, delta, theta, vega, gamma, ivvwap, vanna, charm, speed, zomma, color, volga, veta, tgr, tv and dtr

# def on_greekSnapshot(msg):
#     gr = apidata_models.Greeks(msg)
#     print(msg) # or print(greeks.gamma) likewise object can be called for kind, ticker, iv, delta, theta, vega, gamma, ivvwap, vanna, charm, speed, zomma, color, volga, veta, tgr, tv and dtr

def connection_started():
    print("Connection started callback")

def connection_stopped():
    print("Connection stopped callback")

event_loop.create_task(main())
try:
   event_loop.run_forever()
finally:
   event_loop.close()
