from django import forms
from .mixins import BootStrapForm
from django.utils.translation import gettext_lazy as _


class AdminLoginForm(BootStrapForm):
    email = forms.EmailField(label=_("Email"), required=True)
    password = forms.CharField(label=_("Password"), required=True, widget=forms.PasswordInput())
    remember = forms.BooleanField(label=_("Remember Me"), required=False)
