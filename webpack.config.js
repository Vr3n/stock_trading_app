const path = require("path");
const webpack = require("webpack");
const Dotenv = require("dotenv-webpack");

module.exports = {
  entry: "./frontend/src/index.tsx",
  output: {
    path: path.resolve(__dirname, "./static/js"),
    publicPath: "/static/js/",
    filename: "[name].js",
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.(sass|css)$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.svg$/,
        use: ["@svgr/webpack"],
      },
      {
        test: /\.(png|jpg)$/,
        use: ["url-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx"],
    alias: {
      Svgs: path.resolve(__dirname, "./frontend/src/assets/svg/"),
      Imgs: path.resolve(__dirname, "./frontend/src/assets/images/"),
    },
  },
  optimization: {
    minimize: true,
  },
  devtool: "source-map",
  devServer: {
    writeToDisk: true,
  },
  plugins: [
    new Dotenv(),
  ],
};
